<div class="container">
<form>
    <?php
        include('action.php');

        $query = "SELECT * FROM donhang, khachhang where khachhang.makh = donhang.makh ORDER BY donhang.ngaygiao DESC";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi sai';
    ?>
    <h3 class="text-center text-info">Danh sách đơn hàng</h3>
    <table class="table table-hover" id="data-table">
        <thead>
        <tr bgcolor="#95f461">
            <th>Mã đơn hàng</th>
            <th>Họ tên khách hàng</th>
            <th>Nhân viên giao</th>
            <th>Nhân viên duyệt</th>
            <th>Ngày đặt</th>
            <th>Ngày giao</th>
            <th>Tình trạng</th>
            <?php
                if($_SESSION['quyen'] != 3) echo "<th>Hành Động</th>";
            ?>
        </tr>
        </thead>          
        <tbody>
        <?php $d=0; while ($row = $result->fetch_assoc()) {$d++;
            if($d%2==1) $bg="#b0e5e5"; else $bg= "white";
            $nvg= $row['nvgiao'];
            $nvd = $row['nvduyet'];
            $q = "SELECT * FROM donhang, nhanvien where '$nvg' = nhanvien.manv";
            $r = $conn->query($q);
            if(!$r) echo 'Cau truy van bi sai';
            $row1 = $r->fetch_assoc();
            if($r->num_rows == 0)  $Giao = "";
            else $Giao = $row1['hotennv'];

            $q2 = "SELECT * FROM donhang, nhanvien where '$nvd' =nhanvien.manv";
            $r2 = $conn->query($q2);
            if(!$r2) echo 'Cau truy van bi sai';
            $row2 = $r2->fetch_assoc();
            if($r2->num_rows == 0)  $Duyet = "";
            else $Duyet = $row1['hotennv'];

            switch($row['tinhtrang'])
            {
                case "1": // đã giao
                    $tt = "Đang duyệt";
                    break;
                case "2": // đang duyệt
                    $tt = "đang giao";
                    break;
                case "3": // đang giao
                    $tt = "Đã hủy";
                    break;
                case "4": // đã hủy
                    $tt = "Đã giao";
                    break;
            }
            ?>
        <tr bgcolor="<?php echo $bg; ?>">
            <td><?= $row['madon']; ?></td>
            <td><?= $row['tenkh']; ?></td>
            <td><?= $Giao; ?></td>
            <td><?= $Duyet; ?></td>
            <td><?= $row['ngaydat']; ?></td>
            <td><?php if($row['ngaygiao'] == "0000-00-00") echo ""; else echo $row['ngaygiao'];?></td>
            <td><?= $tt; ?></td>
            <?php
            if($_SESSION['quyen'] != 3)
            {
                $md = $row['madon'];
                echo "<td><a href='action.php?chitietDon=$md' class='badge badge-primary p-2'>Chi tiết</a></td>";
            }
            ?> 
        </tr>
        <?php } ?>
        </tbody>
    </table>
</form>
</div>