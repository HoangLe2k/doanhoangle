
<div class="container">
    <?php include('action.php') ?>
    <div class="row">
    <h3  class="text-justify-center text-info">Nhập thông tin của nhân viên mới</h3>
    <form weight=50% action="action.php" method="post" >        
        <div class="form-group">
        <input type="text" name="hotennv" class="form-control" value="<?php if(isset($Tennv)) echo $Tennv;?>" placeholder="Nhập họ và tên" >
        </div>
        <div class="form-group">
        Nam<input type="radio" name="gioitinh" value="1">&nbsp;Nữ<input type="radio" name="gioitinh" value="0">
        </div>
        <div class="form-group">
        <input type="text" name="email" class="form-control" value="<?php if(isset($Email)) echo $Email;?>" placeholder="Nhập email nhân viên" >
        </div>
        <div class="form-group">
        <input type="number" name="sdt" class="form-control" value="<?php if(isset($Sdt)) echo $Sdt;?>" placeholder="Nhập số điện thoại" >
        </div>
        <div class="form-group">
        Quản trị viên<input type="radio" name="quyen" value="1">&nbsp;Nhân viên CSKH<input type="radio" name="quyen" value="2">&nbsp;Nhân viên giao hàng<input type="radio" name="quyen" value="3">
        </div>
        <div class="form-group">
        <input type="text" name="tendangnhap" class="form-control" value="<?php if(isset($Tentk)) echo $Tentk;?>" placeholder="Tạo tên đăng nhập" >
        </div>
        <div class="form-group">
        <input type="password" name="matkhau" class="form-control" value="<?php if(isset($Pas)) echo $Pas;?>" placeholder="Tạo mật khẩu" >
        </div>
        <div class="form-group">
        <input type="password" name="rematkhau" class="form-control" value="<?php if(isset($Repas)) echo $Repas;?>" placeholder="Nhập lại mật khẩu" >
        </div>
        <?php
        if(isset($_SESSION['thongbaoAddNV']))
        {
            echo '<div class="form-group">
            <span style="color:red">'.$_SESSION['thongbaoAddNV'].'</span>
            </div>';
            unset($_SESSION['thongbaoAddNV']);
        }
        ?>
        <div class="form-group">
        <input type="submit" name="addNhanvien" class="btn btn-primary btn-block" value="Thêm Nhân viên">            
        </div>
    </form>
    </div>
</div>