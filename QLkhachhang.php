<div class="container">
<form method="POST" action="action.php">
    <div class="form-group">
        <table>
            <tr>
                <td colspan =3><input size=200px type="text" name="timQLKH" class="form-control" placeholder="Tìm kiếm..."></td>
                <td><input  type="submit" name="timkiemQLKH" class="btn btn-primary btn-block" value="Tìm kiếm"></td>
            </tr>
        </table>
                
    </div>
    <h3 class="text-center text-info">Danh sách khách hàng</h3>
    <?php
        include('action.php');
        if(isset($_SESSION['thongbaoQlKH']))
        {
            echo '<div class="form-group">
            <span style="color:red">'.$_SESSION['thongbaoQlKH'].'</span>
            </div>';
            unset($_SESSION['thongbaoQlKH']);
        }
        if(isset($_SESSION['timkiemQLKH']))
        {
            $key = $_SESSION['timkiemQLKH'];
            $query = "SELECT * FROM khachhang WHERE makh like '$key' or tenkh like '$key' or email_kh like '$key' or sdt like '$key' or diachi like '$key' or tendangnhap like '$key'";
            unset($_SESSION['timkiemQLKH']);
        } 
        else
        {
            $query = "SELECT * FROM khachhang";
        }
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi sai';
        ?>
    <table class="table table-hover" id="data-table">
        <thead>
        <tr bgcolor="#95f461">
            <th>Mã khách hàng</th>
            <th>Họ tên kkhách hàng</th>
            <th>Giới tính</th>
            <th>Email</th>
            <th>Số điện thoại</th>
            <th>Địa chỉ</th>
            <th>Tên đăng nhập</th>
            <th>Mật Khẩu</th>
            <?php
                if($_SESSION['quyen'] != 3) echo "<th>Hành Động</th>";
            ?>
        </tr>
        </thead>          
        <tbody>
        <?php $gt="";$d=0; while ($row = $result->fetch_assoc()) {$d++;
            if($d%2==1) $bg="#b0e5e5"; else $bg= "white";
            if($row['gioitinh']==1) $gt="Nam"; else $gt="Nữ";
            ?>
        <tr bgcolor="<?php echo $bg; ?>">
            <td><?= $row['makh']; ?></td>
            <td><?= $row['tenkh']; ?></td>
            <td><?= $gt; ?></td>
            <td><?= $row['email_kh']; ?></td>
            <td><?= $row['sdt']; ?></td>
            <td><?= $row['diachi']; ?></td>
            <td><?= $row['tendangnhap']; ?></td>
            <td><?= $row['matkhau']; ?></td>
            <?php
            if($_SESSION['quyen'] != 3)
            {
                $mkh = $row['makh'];
                echo "<td><a href='action.php?chitietKH=$mkh' class='badge badge-primary p-2'>Chi tiết</a> 
                <a style='background-color: #fc3232;' href='action.php?xoaKH=$mkh' class='badge badge-primary p-2'>Xóa KH</a></td>";
            } 
            ?>  
        </tr>
        <?php } ?>
        </tbody>
    </table>
    
</form>
</div>