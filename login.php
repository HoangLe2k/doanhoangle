<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block"><img width="100%" height="100%" src="images/14.jpg"></div>
                            <div class="col-lg-6">
                                <form method="POST" action="action.php">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4"><a href="index.php" class="logo"><img width="100px" height="20px" src="images/logo.png"></img></a> Welcome!</h1>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="text" required name="tk" class="form-control form-control-user"
                                                value="<?php
                                                if( isset($_COOKIE["tk"])) echo $_COOKIE["tk"];?>"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Nhập Tên đăng nhập...">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" required name="mk" class="form-control form-control-user"
                                                value="<?php
                                                if( isset($_COOKIE["mk"])) echo $_COOKIE["mk"];?>"
                                                id="exampleInputPassword" placeholder="Mật khẩu...">
                                            <?php
                                                if(isset($_GET['mes']) and $_GET['mes'] ==1) 
                                                echo "<h style='color:red'>(*)Sai tài khoảng hoặc mật khẩu!</h>";
                                            ?>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="ghinho" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Ghi nhớ</label>
                                            </div>
                                        </div>
                                        <input class="btn btn-primary btn-user btn-block" type="submit" name="dangnhap" value="Đăng Nhập">                                        
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="forgot-password.html">Quên mật khẩu?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="Register.php">Bạn chưa đăng ký? Đăng ký ngay!</a>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>