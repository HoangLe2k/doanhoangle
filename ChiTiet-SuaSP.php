
<?php
    include('action.php');
    $Masp = $_SESSION['masp'];
    $query = "SELECT * FROM sanpham,nhomsp where sanpham.manhom = nhomsp.manhom and masp = '$Masp'";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $row = $result->fetch_assoc();
?>
<div class="row">
    
    
    <div class="container">
    <center><h3  class="text-justify-center text-info">Nhập thông tin sản phẩm</h3></center>
        <div class="row">        
        <form action="action.php" method="post" enctype="multipart/form-data">
            <div class="form-group">    
            <center><img style="width: 400px" src="./images/<?=$row['hinh'];?>"></center>
            </div>
            <div class="form-group">
            <span>Mã sản phẩm</span>
            <input readonly disabled type="text" class="form-control" value="<?= $row['masp'] ?>" >
            </div>
            <div class="form-group">
            <span>Tên sản phẩm</span>
            <input type="text" name="tensp" class="form-control" value="<?= $row['tensp'] ?>" >
            </div>
            <div class="form-group">
            <span>Đơn giá</span>
            <input required type="number" name="dongia" class="form-control" value="<?= $row['dongia'] ?>" >
            </div>
            <div class="form-group">
            <span>Mô tả sản phẩm</span>
            <input required type="text" name="mota" class="form-control" value="<?= $row['mota'] ?>" >
            </div>
            <div class="form-group">
            <span>Số lượng trong kho</span>
            <input required type="number" name="soluong" class="form-control" value="<?= $row['soluong'] ?>" >
            </div>
            <div class="form-group">
            <span>Nhóm sản phẩm</span>
            <?php
            $query2 = "SELECT * FROM nhomsp";
            $result2 = $conn->query($query2);
            if(!$result2) echo 'Cau truy van bi sai';
            ?>
            <select name="nhomsp">
                <option value="<?= $row['manhom'] ?>" selected="selected"><?= $row['tennhom']?></option>
                <?php while ($row2 = $result2->fetch_assoc()) { ?>
                    <option value="<?= $row2['manhom'] ?>" ><?= $row2['tennhom'] ?></option>
                <?php } ?>
            </select>
            </div>
            <div class="form-group">
            <span>Hình ảnh</span><input type="hidden" name="hinhc" value="<?=$row['hinh'];?>">
                <br><input type="file" name="hinh" class="custom-file">
            </div>
            
            <a href='adminHome.php?loadpage=QLSanpham.php' class='badge badge-primary p-2'>Quay về</a>
            <input type="submit" name="suaSP" style='background-color: #6be56d;' value="Lưu thay đổi">        
            <a style='background-color: #fc3232;' href='action.php?xoaSP=<?=$Masp;?>' class='badge badge-primary p-2'>Xóa sp</a>              
        </form>
        </div>
    </div>
</div>