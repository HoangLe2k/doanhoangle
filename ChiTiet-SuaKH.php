
<div class="container">
    <?php include('action.php') ?>
    <div class="row">
    <h3  class="text-justify-center text-info">Thông tin của khách hàng</h3>
    <?php
    $Mkh = $_SESSION['mkh'];
    $query = "SELECT * FROM khachhang where makh = '$Mkh'";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $row = $result->fetch_assoc();
    ?>
    <form action="action.php" method="post" >
        <div class="form-group">
        <span>Mã khách hàng</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['makh'] ?> ">
        </div>    
        <div class="form-group">
        <span>Họ tên khách hàng</span>
        <input type="text" name="tenkh" class="form-control" value="<?= $row['tenkh'] ?> ">
        </div>
        <div class="form-group">
        <span>Giới tính</span>
        Nam<input type="radio" name="gioitinh" <?php if($row['gioitinh']==1) echo "checked" ?> value="1">&nbsp;Nữ<input type="radio" name="gioitinh" <?php if($row['gioitinh']==0) echo "checked" ?> value="0">
        </div>        
        <div class="form-group">
        <span>Email</span>
        <input type="text" name="email" class="form-control" value="<?= $row['email_kh'] ?>">
        </div>
        <div class="form-group">
        <span>Số điện thoại</span>
        <input type="text" name="sdt" class="form-control" value="<?= $row['sdt'] ?>">
        </div>
        <div class="form-group">
        <span>Địa chỉ khách hàng</span>
        <input type="text" name="diachi" class="form-control" value="<?= $row['diachi'] ?>">
        </div>
        <div class="form-group">
        <span>Tên đăng nhập</span>
        <input type="text" name="tendangnhap" class="form-control" value="<?= $row['tendangnhap'] ?>">
        </div>
        <div class="form-group">
        <span>Mật Khẩu</span>
        <input type="text" name="matkhau" class="form-control" value="<?= $row['matkhau'] ?>" >
        </div>
        <div class="form-group">
        <a href='adminHome.php?loadpage=QLkhachhang.php' class='badge badge-primary p-2'>Quay về</a>
        <input type="submit" name="suaKH" style='background-color: #6be56d;' value="Lưu thay đổi">        
        <a style='background-color: #fc3232;' href='action.php?xoaKH=<?=$Mkh;?>' class='badge badge-primary p-2'>Xóa KH</a>              
        </div>
    </form>
    </div>
</div>