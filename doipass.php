<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thay đổi mật khẩu (kh)</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
    
<!-- header section starts      -->
<?php
include('action.php');
if(isset($_GET['error']))
{
    echo '<script type="text/javascript">alert("'.'Mật khẩu không khớp!'.'")</script>';
}
?>
<header>

    <a href="#" class="logo"><img width="100px" height="20px" src="images/logo.png"></img></a>

    <nav class="navbar">
        <a class="active" href="index.php#home">Trang Chủ</a>
        <a href="index.php#dishes">Món Ăn</a>
        <a href="index.php#about">Thông Tin</a>
        <a href="index.php#review">Đánh Giá</a>
        
    </nav>

    <div class="icons">
        <i class="fas fa-bars" id="menu-bars"></i>
        <?php
            if(isset($_SESSION['makh']))
            {
                echo "<span style='font-size: 20px;' color='#27ae60'>".$_SESSION['tenkh']."</span>";
                echo '<a href="#" class="fas fa-user-alt"></a>';
                echo '<a href="action.php?logout" class="fas fa-sign-out-alt"></a>';
            }
            else
            {
                echo '<a href="login.php" class="fas fa-user-alt"></a>';
            }

        ?>   
        
        
    </div>

</header>

<!-- header section ends-->
<?php

?>
<section style="padding-top: 100px; padding-bottom: 100px;" class="body">
<center>
    <form method="POST" action="action.php">
        
        <table class="table table-hover" id="data-table">
        <tr>
            <th colspan=2><center><h1 >Thay đổi mật khẩu</h1></center></th>
        </tr>
        <tr><td style="width:650px"><h2 style="text-align: right;">Mật khẩu mới:</h2></td> <td><input style="text-align: left; height: 25px;width: 200px;" name="newpass" type="password" required ></td></tr>
        <tr><td style="width:650px"><h2 style="text-align: right;">Nhập lại mật khẩu:</h2></td> <td><input style="text-align: left; height: 25px;width: 200px;" name="renewpass" type="password" required ></td></tr>
        </table>
        <div class="form-group">
        <input class="btn btn-primary btn-block" type="submit" name="DoiPass" value="Đổi" >
        </div>
    </form>
</center>

</section>


<!-- footer section starts  -->

<section class="footer">

    <div class="box-container">

        <div class="box">
            <h3>locations</h3>
            <a href="#">Trường Đại học Nha Trang</a>
            <a href="#">Khoa Công nghệ thông tin</a>
            <a href="#">Môn phát triển UD mã nguồn mở</a>
        </div>

        <div class="box">
            <h3>quick links</h3>
            <a href="index.php#home">Trang Chủ</a>
            <a href="index.php#dishes">Món Ăn</a>
            <a href="index.php#about">Thông Tin</a>
            <a href="index.php#review">Đánh Giá</a>
        </div>

        <div class="box">
            <h3>Author info</h3>
            <a href="#">SV: Lê Nguyễn Việt Hoàng</a>
            <a href="#">MSSV: 60131564</a>
            <a href="#">email:hoang.lnv.60cntt@ntu.edu.vn</a>
            <a href="#">Lớp:60cntt-2</a>
        </div>

        <div class="box">
            <h3>follow me</h3>
            <a href="#">facebook</a>
            <a href="#">twitter</a>
            <a href="#">instagram</a>
            <a href="#">linkedin</a>
        </div>

    </div>

    <div class="credit">Template gốc từ copyright @ 2021 by <span>mr. web designer</span> </div>

</section>

<!-- footer section ends -->

<!-- loader part  -->

<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<!-- custom js file link  -->
<script src="js/script.js"></script> 

</body>
</html>