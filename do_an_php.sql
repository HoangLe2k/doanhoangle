-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 26, 2021 at 08:58 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `do_an_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `chitietdonhang`
--

CREATE TABLE `chitietdonhang` (
  `madon` varchar(6) NOT NULL,
  `masp` varchar(6) NOT NULL,
  `dh_soluong` tinyint(4) NOT NULL,
  `dh_giaban` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chitietdonhang`
--

INSERT INTO `chitietdonhang` (`madon`, `masp`, `dh_soluong`, `dh_giaban`) VALUES
('DH003', 'SP11', 1, 120000),
('DH003', 'SP12', 1, 85000),
('DH003', 'SP23', 2, 15000),
('HD001', 'SP01', 2, 30000),
('HD001', 'SP09', 2, 30000),
('HD001', 'SP11', 1, 120000),
('HD001', 'SP24', 2, 15000),
('HD002', 'SP05', 3, 70000),
('HD002', 'SP12', 1, 85000),
('HD002', 'SP21', 1, 15000),
('HD002', 'SP22', 1, 15000);

-- --------------------------------------------------------

--
-- Table structure for table `donhang`
--

CREATE TABLE `donhang` (
  `madon` varchar(6) NOT NULL,
  `makh` varchar(6) NOT NULL,
  `nvgiao` varchar(50) NOT NULL,
  `nvduyet` varchar(50) NOT NULL,
  `ngaydat` date NOT NULL,
  `ngaygiao` date NOT NULL,
  `tinhtrang` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `donhang`
--

INSERT INTO `donhang` (`madon`, `makh`, `nvgiao`, `nvduyet`, `ngaydat`, `ngaygiao`, `tinhtrang`) VALUES
('DH003', 'KH01', 'NV04', 'NV02', '2021-10-24', '2021-10-26', '2'),
('HD001', 'KH01', 'NV05', 'NV02', '2021-10-01', '2021-10-02', '4'),
('HD002', 'KH03', 'NV04', 'NV02', '2021-10-11', '2021-10-11', '4');

-- --------------------------------------------------------

--
-- Table structure for table `giohang`
--

CREATE TABLE `giohang` (
  `makh` varchar(6) NOT NULL,
  `masp` varchar(6) NOT NULL,
  `soluong_gh` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `makh` varchar(6) NOT NULL,
  `tenkh` varchar(50) NOT NULL,
  `gioitinh` tinyint(2) NOT NULL,
  `email_kh` varchar(50) NOT NULL,
  `sdt` varchar(15) NOT NULL,
  `diachi` varchar(100) NOT NULL,
  `tendangnhap` varchar(100) NOT NULL,
  `matkhau` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`makh`, `tenkh`, `gioitinh`, `email_kh`, `sdt`, `diachi`, `tendangnhap`, `matkhau`) VALUES
('KH01', 'Tô Phước Thái 1111  ', 1, 'thaitp@gmail.com', '0378549632', '20 Tôn Thất Tùng, Nha Trang', 'thaiphuoc', '778d628ba752a05fb297fe44029cabd1'),
('KH03', 'Trần Xuân Phước ', 1, 'phuocxuan@gmail.com', '0124785632', '660 Chợ Đầm, Nha Trang', 'xuanphuoc', '778d628ba752a05fb297fe44029cabd1'),
('KH04', 'Lê Long Thành ', 1, 'thanhll@gmail.com', '0192489632', '18 Hoàng Diệu, Nha Trang', 'thanhlelong', '778d628ba752a05fb297fe44029cabd1'),
('KH05', 'Đào Long Thiên ', 1, 'longthien@gmail.com', '0321499632', '77 Hòn Chồng, Nha Trang', 'longthien123', '778d628ba752a05fb297fe44029cabd1'),
('KH06', 'khách hàng a', 1, 'ahsgv', '273465', 'habsgvg', 'abc', 'd8c7408a67a89e5bd43ece16b524f904');

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `manv` varchar(6) NOT NULL,
  `hotennv` varchar(50) NOT NULL,
  `gioitinh` tinyint(2) NOT NULL,
  `email_nv` varchar(100) NOT NULL,
  `sdt` varchar(15) NOT NULL,
  `quyen` tinyint(4) NOT NULL,
  `tendangnhap` varchar(50) NOT NULL,
  `matkhau` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`manv`, `hotennv`, `gioitinh`, `email_nv`, `sdt`, `quyen`, `tendangnhap`, `matkhau`) VALUES
('NV02', 'Chế Đức Tài Tài 1111  ', 1, 'taicd@gmail.com', '0923651269', 2, 'taiducche', 'd8c7408a67a89e5bd43ece16b524f904'),
('NV03', 'Lê Nguyễn Việt Hoàng ', 1, 'hoang@gmail.com', '0581236549', 1, 'hoangadmin', '9d6c12d0b2276bd579283c4448e5525e'),
('NV04', 'Nguyễn Phương Thanh  ', 0, 'thanhnp@gmail.com', '0586123652', 3, 'thanhddon', '43ee055fe909380dec779c02f291d425'),
('NV05', 'Nguyễn Trọng Nghĩa ', 1, 'nghiant@gmail.com', '0945128745', 3, 'nghiaddon', '9d6c12d0b2276bd579283c4448e5525e'),
('NV08', 'rêr ', 1, 'jgb', '362526', 1, 'tyu', '50d02cedd8c4fa791a8c52efac74eb70'),
('NV09', 'hoàng again', 1, 'a@gmail.com', '12343124', 1, 'hoang', '9d6c12d0b2276bd579283c4448e5525e');

-- --------------------------------------------------------

--
-- Table structure for table `nhomsp`
--

CREATE TABLE `nhomsp` (
  `manhom` varchar(6) NOT NULL,
  `tennhom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nhomsp`
--

INSERT INTO `nhomsp` (`manhom`, `tennhom`) VALUES
('NSP01', 'Gà'),
('NSP02', 'Hamburger'),
('NSP03', 'Bánh mì'),
('NSP04', 'Pizza'),
('NSP05', 'Sandwich'),
('NSP06', 'Spaghetti'),
('NSP07', 'Xúc xích'),
('NSP08', 'Đồ uống'),
('NSP09', 'Khoai tây'),
('NSP10', 'Hành tây'),
('NSP12', 'heo2'),
('NSP14', 'sdsdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE `sanpham` (
  `masp` varchar(6) NOT NULL,
  `tensp` varchar(50) NOT NULL,
  `dongia` int(11) NOT NULL,
  `mota` varchar(300) NOT NULL,
  `soluong` int(11) NOT NULL,
  `hinh` varchar(100) NOT NULL,
  `manhom` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`masp`, `tensp`, `dongia`, `mota`, `soluong`, `hinh`, `manhom`) VALUES
('SP01', 'Cánh gà nướng', 30000, 'Cánh gà nướng với gia vị đặt biệt của shop sẽ không làm các bạn thất vọng !', 0, 'canh_ga_nuong.jpg', 'NSP01'),
('SP02', 'Cánh gà rán', 35000, 'cánh gà được rán vàng ruộm, trong thấm gia vị ngoài giòn tan !', 0, 'canh_ga_ran.jpg', 'NSP01'),
('SP03', 'Đùi gà nướng', 55000, 'phần đùi gà được nướng với gia vị đặc biệt của shop !', 0, 'dui_ga_nuong.jpg', 'NSP01'),
('SP04', 'Đùi gà rán', 65000, 'đùi gà được rán giòn tan với nguyên liệu tươi ngon !', 3, 'dui_ga_ran.jpg', 'NSP01'),
('SP05', 'Gà sốt cay', 70000, 'gà được sốt với loại sốt cay đặc biệt của shop !', 5, 'ga_sot_cay.jpg', 'NSP01'),
('SP06', 'Hamburger bò', 70000, 'Hamburger bò được làm từ các nguyên liệu tươi ngon !', 3, 'hamburger_bo.jpg', 'NSP02'),
('SP07', 'Hamburger cá ngừ', 80000, 'Hamburger cá ngừ được làm từ các nguyên liệu tươi ngon !', 5, 'hamburger_ca_ngu.jpg', 'NSP02'),
('SP08', 'Bánh mì kẹp thịt', 40000, 'bánh mì kẹp thịt  được làm từ các nguyên liệu tươi ngon !', 2, 'banh_mi_kep_thit.jpg', 'NSP03'),
('SP09', 'Bánh mì kẹp xúc xích', 30000, 'bánh mì kẹp xúc xích được làm từ các nguyên liệu tươi ngon !', 4, 'banh_mi_kep_xuc_xich.jpg', 'NSP03'),
('SP10', 'Pizza phô mai', 70000, 'Pizza phô mai được làm từ các nguyên liệu tươi ngon !', 6, 'pizza_pho_mai.jpg', 'NSP04'),
('SP11', 'Pizza hải sản', 120000, 'Pizza hải sản được làm từ các nguyên liệu tươi ngon !', 3, 'pizza_hai_san.jpg', 'NSP04'),
('SP12', 'Pizza nấm', 85000, 'Pizza nấm được làm từ các nguyên liệu tươi ngon !', 3, 'pizza_nam.jpg', 'NSP04'),
('SP13', 'Pizza xúc xích', 95000, 'Pizza xúc xích được làm từ các nguyên liệu tươi ngon !', 2, 'pizza_xuc_xich.jpg', 'NSP04'),
('SP14', 'Sandwich gà', 35000, 'Sandwich gà được làm từ các nguyên liệu tươi ngon !', 3, 'sandwich_ga.jpg', 'NSP05'),
('SP15', 'Sandwich thịt thăn', 40000, 'Sandwich thịt thăn được làm từ các nguyên liệu tươi ngon !', 2, 'sandwich_thit_than.jpg', 'NSP05'),
('SP16', 'Sandwich trứng phô mai', 20000, 'Sandwich trứng phô mai được làm từ các nguyên liệu tươi ngon !', 3, 'sandwich_trung_pho_mai.jpg', 'NSP05'),
('SP17', 'Spaghetti sốt cà chua bò', 55000, 'Spaghetti sốt cà chua bò được làm từ các nguyên liệu tươi ngon !', 3, 'spaghetti_sot_ca_chua_bo_bam.jpg', 'NSP06'),
('SP18', 'Spaghetti sốt kem', 50000, 'Spaghetti sốt kem được làm từ các nguyên liệu tươi ngon !', 4, 'spaghetti_sot_kem.jpg', 'NSP06'),
('SP19', 'Xúc xích nướng', 20000, 'Xúc xích nướng được làm từ các nguyên liệu tươi ngon !', 3, 'xuc_xich.jpg', 'NSP07'),
('SP20', 'Khoai tây chiên', 35000, 'khoai tây chiên vàng ruộm, giòn tan với gia vị đặc biệt !', 3, 'khoai_tay_chien.jpg', 'NSP09'),
('SP21', 'Hành tây chiên', 30000, 'hành tây chiên là món đặc biệt của shop !', 4, 'hanh_tay_chien.jpg', 'NSP10'),
('SP22', 'Sprite', 15000, 'Nước ngọt vị chanh có gas !', 5, 'sprite.jpg', 'NSP08'),
('SP23', 'Fanta', 15000, 'nước ngọt vị cam không có gas !', 5, 'fanta.jpg', 'NSP08'),
('SP24', 'Pepsi', 15000, 'nước ngọt có gas !', 5, 'pepsi.jpg', 'NSP08'),
('SP25', 'test', 2147483647, 'đây là sản phẩm test', 0, '123.jpg', 'NSP14');

-- --------------------------------------------------------

--
-- Table structure for table `tinhtrangdon`
--

CREATE TABLE `tinhtrangdon` (
  `tinhtrang` varchar(5) NOT NULL,
  `mota` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tinhtrangdon`
--

INSERT INTO `tinhtrangdon` (`tinhtrang`, `mota`) VALUES
('1', 'chờ xác nhận'),
('2', 'đã xác nhận'),
('3', 'đang vận chuyển'),
('4', 'đã giao '),
('5', 'đã hủy đơn');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chitietdonhang`
--
ALTER TABLE `chitietdonhang`
  ADD PRIMARY KEY (`madon`,`masp`),
  ADD KEY `FK_MaSP` (`masp`);

--
-- Indexes for table `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`madon`),
  ADD KEY `FK_TTDON` (`tinhtrang`),
  ADD KEY `KF_MaNVD` (`nvduyet`),
  ADD KEY `KF_MaNVG` (`nvgiao`),
  ADD KEY `FK_MaKH` (`makh`);

--
-- Indexes for table `giohang`
--
ALTER TABLE `giohang`
  ADD PRIMARY KEY (`makh`,`masp`),
  ADD KEY `FK_GH_MSP` (`masp`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`makh`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`manv`);

--
-- Indexes for table `nhomsp`
--
ALTER TABLE `nhomsp`
  ADD PRIMARY KEY (`manhom`);

--
-- Indexes for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`masp`),
  ADD KEY `FK_MaNHOMSP` (`manhom`);

--
-- Indexes for table `tinhtrangdon`
--
ALTER TABLE `tinhtrangdon`
  ADD PRIMARY KEY (`tinhtrang`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chitietdonhang`
--
ALTER TABLE `chitietdonhang`
  ADD CONSTRAINT `FK_MaSP` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`),
  ADD CONSTRAINT `KF_MaDON` FOREIGN KEY (`madon`) REFERENCES `donhang` (`madon`);

--
-- Constraints for table `donhang`
--
ALTER TABLE `donhang`
  ADD CONSTRAINT `FK_MaKH` FOREIGN KEY (`makh`) REFERENCES `khachhang` (`makh`),
  ADD CONSTRAINT `FK_TTDON` FOREIGN KEY (`tinhtrang`) REFERENCES `tinhtrangdon` (`tinhtrang`);

--
-- Constraints for table `giohang`
--
ALTER TABLE `giohang`
  ADD CONSTRAINT `FK_GH_MKH` FOREIGN KEY (`makh`) REFERENCES `khachhang` (`makh`),
  ADD CONSTRAINT `FK_GH_MSP` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`);

--
-- Constraints for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `FK_MaNHOMSP` FOREIGN KEY (`manhom`) REFERENCES `nhomsp` (`manhom`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
