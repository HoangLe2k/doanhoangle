<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
    
<!-- header section starts      -->
<?php
include('action.php');
if(isset($_GET['cd']))
{
    echo '<script type="text/javascript">alert("'.'Chốt đơn thành công!'.'")</script>';
}
if(isset($_GET['error']))
{
    echo '<script type="text/javascript">alert("'.'Quý khách chưa chọn sản phẩm muốn mua!'.'")</script>';
}
?>
<header>

    <a href="#" class="logo"><img width="100px" height="20px" src="images/logo.png"></img></a>

    <nav class="navbar">
        <a class="active" href="#home">Trang Chủ</a>
        <a href="#dishes">Món Ăn</a>
        <a href="#about">Thông Tin</a>
        <a href="#review">Đánh Giá</a>
    </nav>

    <div class="icons">
        <i class="fas fa-bars" id="menu-bars"></i>
        <?php
            if(isset($_SESSION['makh']))
            {
                echo "<span style='font-size: 20px;' color='#27ae60'>".$_SESSION['tenkh']."</span>";
                echo '<a href="ProfileKH.php" class="fas fa-user-alt"></a>';
                echo '<a href="action.php?goGioHang" class="fas fa-shopping-cart"></a>';
                echo '<a href="action.php?logout" class="fas fa-sign-out-alt"></a>';
            }
            else
            {
                echo '<a href="login.php" class="fas fa-user-alt"></a>';
            }

        ?>   
        
        
    </div>

</header>

<!-- header section ends-->


<!-- home section starts  -->

<section class="home" id="home">

    <div style="padding-top: 100px;" id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">

        <div class="carousel-inner">

            <div class="carousel-item active">
                <div class="row">
                <div class="image col-md-5">
                    <img src="images/home-img-1.png" alt="">
                </div>
                <div class="content col-md-7">
                    <span class="spannn">Những món ăn hàng đầu!</span>
                    <h3 class="hh3">spaghetti</h3>
                    <p class="pp">Là một món ăn đến từ Ý, với các nguyên liệu tươi ngon chắc chắn bạn sẽ không thể bỏ lỡ!</p>
                </div>
                </div>
            </div>

            <div class="carousel-item">
                <div class="row">
                    <div class="image col-md-5">
                        <img src="images/home-img-2.png" alt="">
                    </div>
                    <div class="content col-md-7">
                    <span class="spannn">Những món ăn hàng đầu!</span>
                        <h3 class="hh3">gà rán</h3>
                        <p class="pp">Là một món ăn rất phổ biến. Nhưng ở đây chúng tôi có những công thức gia vị đặc biệt có thể bạn sẽ thích nó!</p>
                    </div>
                </div>
            </div>

            <div class="carousel-item">
                <div class="row">
                    <div class="image col-md-5">
                        <img src="images/home-img-3.png" alt="">
                    </div>
                    <div class="content col-md-7">
                        <span class="spannn">Những món ăn hàng đầu!</span>
                        <h3 class="hh3">pizza</h3>
                        <p class="pp">Là món ăn mang phong cách mới lạ, kết hợp giữa nền ẩm thực VIỆT NAM và ý. chắc chắn bạn  sẽ không bỏ qua!</p>
                    </div>
                </div>
            </div>

        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>

    </div>

</section>

<!-- home section ends -->

<!-- dishes section starts  -->

<section class="dishes" id="dishes">

    <h3 class="sub-heading">MENU</h3>
    <h1 class="heading"> NHỮNG MÓN ĂN CHÚNG TÔI ĐANG KINH DOANH </h1>
    <div class="row">
    <div class="col-md-2">
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">Tất cả</a>
        </div>
        </nav>
    <?php
    $query = "SELECT * FROM nhomsp";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $numRows = mysqli_num_rows($result);
    // tổng số trang hiển thị
    
    if($result->num_rows !=0)
    {
        while($row = $result->fetch_array())
        { ?>
        <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php?manhom=<?= $row['manhom']; ?>"><?= $row['tennhom'] ?></a>
        </div>
        </nav>
      <?php  }
    }
?>
        <!-- As a link -->
        
        
    </div>
    <div class="box-container col-md-10">

<?php
    if(!isset($_GET['page'])){
        $_GET['page'] = 1;
    }
    $rowPerPage = 8;
    // vị trí của mẩu tin đầu tiên trên mỗi trang
    $offset = ($_GET['page'] - 1) * $rowPerPage;

    if(isset($_GET['manhom']))
    {
        $manhom = $_GET['manhom'];
        $query = "SELECT* FROM sanpham WHERE manhom = '$manhom' and sanpham.soluong > 0 LIMIT $offset, $rowPerPage";
    }
    else $query = "SELECT* FROM sanpham WHERE sanpham.soluong > 0 LIMIT $offset, $rowPerPage";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $numRows = mysqli_num_rows($result);
    // tổng số trang hiển thị
    $maxPage = ceil($numRows / $rowPerPage);
    
    if($result->num_rows !=0)
    {
        while($row = $result->fetch_array())
        { $masp = $row['masp'];  ?>
        <div class="box">
            <a href="ChitietSPIndex.php?masp=<?=$masp?>" class="fas fa-eye"></a>
            <img src="images/<?= $row['hinh'] ?>" alt="">
            <h3><?= $row['tensp'] ?></h3>
            <span><?= $row['dongia'] ?> VND</span>
            <a href="action.php?sp=<?= $row['masp']; ?>" class="btn">Đặt hàng</a>
        </div>
      <?php  }
    }
?>
    </div>
    </div>
</section>
<center>
    <nav class="container">
    <div class="row">
    <ul class="pagination col-md-12">
        <?php
            if(isset($_GET['manhom']))
            {
                $q = "SELECT* FROM sanpham WHERE manhom = '$manhom' and sanpham.soluong > 0";
            }
            else $q = "SELECT* FROM sanpham WHERE sanpham.soluong > 0";
            $r = $conn->query($q);
            $numRows = mysqli_num_rows($r);
            $maxPage = ceil($numRows/$rowPerPage);            
            if ($_GET['page'] > 1){
                echo '<li class="page-item"><a class="page-link" href="index.php?page='.($_GET['page'] - 1).'"> Previous </a></li>'; //gắn thêm nút Back
            }
            for ($i=1 ; $i<=$maxPage ; $i++)
            {
                if ($i == $_GET['page'])
                {
                    echo '<li class="page-item active"><a class="page-link" href="#">'.$i.'</a></li>'; //trang hiện tại sẽ được bôi đậm
                }
                else echo '<li class="page-item"><a class="page-link" href="index.php?page='.$i.'">'.$i.'</a></li>';
            }
            if ($_GET['page'] < $maxPage) {
                echo '<li class="page-item"><a class="page-link" href="index.php?page='.($_GET['page'] + 1).'"> Next </a></li>';  //gắn thêm nút Next
            }
            $conn->close();
        ?>
        </ul>
        </div>
    </nav>
    </center>

<!-- dishes section ends -->

<!-- about section starts  -->

<section class="about" id="about">

    <h3 class="sub-heading">Thông tin</h3>
    <h1 class="heading"> Bạn sẽ được gì khi đặt hàng của chúng tôi! </h1>

    <div class="row">

        <div class="image">
            <img src="images/about-img.png" alt="">
        </div>

        <div class="content">
            <h3>Đồ ăn ngon nhất NHA TRANG</h3>
            <p>Tọa lạc tại Trung tâm thành phố! chúng tôi có đầy đủ những gì bạn cần với những nguyên liệu tươi, mới , đảm bảo an toàn vệ sinh thực phẩm</p>
            <p>Phương châm hàng đầu của chúng tôi là chất lượng phục vụ, bạn hãy liên hệ ngay với chúng tôi nếu cảm thấy khong hài lòng về chúng tôi. nếu bạn cảm thấy hài lòng thì hãy ủng hộ chúng tôi nhé!</p>
            <p>Phương châm 3 tốt!</p>
            <div class="icons-container">
                <div class="icons">
                    <i class="fas fa-shipping-fast"></i>
                    <span>Giao hàng tốt</span>
                </div>
                <div class="icons">
                    <i class="fas fa-dollar-sign"></i>
                    <span>thanh toán tốt</span>
                </div>
                <div class="icons">
                    <i class="fas fa-headset"></i>
                    <span>chăm sóc tốt</span>
                </div>
            </div>
        </div>

    </div>

</section>

<!-- about section ends -->

<!-- menu section ends -->

<!-- review section starts  -->

<section class="review" id="review">

    <h3 class="sub-heading"> Đánh giá </h3>
    <h1 class="heading"> Khách hàng nói gì về chúng tôi! </h1>

    <div class="review-slider swiper-container ">

        <div class="swiper-wrapper">

            <div style="flex-shrink: 1;margin-left: 10px;height: 240px;" class="swiper-slide slide">
                <i class="fas fa-quote-right"></i>
                <div class="user">
                    <img src="images/pic-1.png" alt="">
                    <div class="user-info">
                        <h3>john dẹo</h3>
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>
                <p>Giao hàng rất nhanh, đồ ăn rất ngon lại còn đang nóng. Nhân viên bán hàng chăm sóc rất tốt, giọng nói của cô ấy rất tuyệt vời!</p>
            </div>

            <div style="flex-shrink: 1;margin-left: 10px;height: 240px;" class="swiper-slide slide">
                <i class="fas fa-quote-right"></i>
                <div class="user">
                    <img src="images/pic-2.png" alt="">
                    <div class="user-info">
                        <h3>Calida</h3>
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>
                <p>chất lượng phụ vụ rất tốt, giao hàng rất nhanh. đồ ăn thì rất ngon nhất là món spaghetti, tối có thể thấy được hương vị quen thuộc từ quê nhà cũng như vị mới lạ từ VIET NAM</p>
            </div>

            <div style="flex-shrink: 1;margin-left: 10px;height: 240px;" class="swiper-slide slide">
                <i class="fas fa-quote-right"></i>
                <div class="user">
                    <img src="images/pic-3.png" alt="">
                    <div class="user-info">
                        <h3>Hùng Lê</h3>
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>
                <p>giao hàng nhanh, đồ ăn ngon hơn những cửa hàng khác mà tôi đã từng thử</p>
            </div>

            <div style="flex-shrink: 1;margin-left: 10px;height: 240px;" class="swiper-slide slide">
                <i class="fas fa-quote-right"></i>
                <div class="user">
                    <img src="images/pic-4.png" alt="">
                    <div class="user-info">
                        <h3>Linh Thùy</h3>
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                    </div>
                </div>
                <p>đồ ăn ở đây ngon lắm luôn, tôi mua đồ ở đây mỗi ngày và giờ tôi không thể ngừng thói quen đó được. nó quá hấp dẫn tôi!</p>
            </div>

        </div>

    </div>
    
</section>

<!-- review section ends -->


<!-- order section ends -->

<!-- footer section starts  -->

<section class="footer">

    <div class="box-container">

        <div class="box">
            <h3>locations</h3>
            <a href="#">Trường Đại học Nha Trang</a>
            <a href="#">Khoa Công nghệ thông tin</a>
            <a href="#">Môn phát triển UD mã nguồn mở</a>
        </div>

        <div class="box">
            <h3>quick links</h3>
            <a href="#home">Trang Chủ</a>
            <a href="#dishes">Món Ăn</a>
            <a href="#about">Thông Tin</a>
            <a href="#review">Đánh Giá</a>
        </div>

        <div class="box">
            <h3>Author info</h3>
            <a href="#">SV: Lê Nguyễn Việt Hoàng</a>
            <a href="#">MSSV: 60131564</a>
            <a href="#">email:hoang.lnv.60cntt@ntu.edu.vn</a>
            <a href="#">Lớp:60cntt-2</a>
        </div>

        <div class="box">
            <h3>follow me</h3>
            <a href="#">facebook</a>
            <a href="#">twitter</a>
            <a href="#">instagram</a>
            <a href="#">linkedin</a>
        </div>

    </div>

    <div class="credit">Template gốc từ copyright @ 2021 by <span>mr. web designer</span> </div>

</section>

<!-- footer section ends -->

<!-- loader part  -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<!-- custom js file link  -->
<script src="./js/script.js"></script> 

</body>
</html>