
<form method="POST" action="action.php">
    <div class="container">
        <h2>BÀI TẬP THỰC HÀNH</h2>
        <table class="table table-hover" id="data-table">
            <tr>
                <td>
                <select name="baitap">
                    <option value="" selected="selected">--Chọn--</option>
                    <option value="Arb1" >Array - bài 1</option>
                    <option value="Arb2" >Array - bài 2</option>
                    <option value="Arb3" >Array - bài 3</option>
                    <option value="Arb4" >Array - bài 4</option>
                    <option value="Arb5" >Array - bài 5</option>
                    <option value="Arb6" >Array - bài 6</option>
                    <option value="Arb7" >Array - bài 7</option>
                    <option value="Fb1" >Form - bài 1</option>
                    <option value="Fb2" >Form - bài 2</option>
                    <option value="Fb3" >Form - bài 3</option>
                    <option value="Fb4" >Form - bài 4</option>
                    <option value="Fb5" >Form - bài 5</option>
                    <option value="Fb6" >Form - bài 6</option>
                    <option value="sql1" >SQL+PHP - bài 2.1</option>
                    <option value="sql2" >SQL+PHP - bài 2.2</option>
                    <option value="sql3" >SQL+PHP - bài 2.3</option>
                    <option value="sql4" >SQL+PHP - bài 2.4</option>
                    <option value="sql5" >SQL+PHP - bài 2.5</option>
                    <option value="sql6" >SQL+PHP - bài 2.6 + 2.7</option>
                    <option value="sql8" >SQL+PHP - bài 2.8</option>
                    <option value="sql9" >SQL+PHP - bài 2.9</option>
                </td>
                <td><input  name="goBaitap" type="submit" value="Mở"></td>
            </tr>
        </table>
    </div>
</form>
<div class="container">
    <table class="table table-hover" id="data-table">
            <tr>
                <td colspan=2>
                    <center>
                        <?php                       
                            if(isset($_SESSION['baitap']))
                            {
                                $loadpage = $_SESSION['baitap'];
                                require_once $loadpage;
                            }
                        ?>  
                    </center>
                </td>
            </tr>
    </table>
</div>