
<div class="container">
    <?php include('action.php');
    $mdh = $_SESSION['mdh'];
    $query = "SELECT * FROM donhang, khachhang where khachhang.makh = donhang.makh and donhang.madon = '$mdh' ";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $row = $result->fetch_assoc();
    ?>
    <div class="row">
    <h3  class="text-justify-center text-info">Duyệt đơn hàng</h3>
    <form weight=50% action="action.php" method="post" >        
        <div class="form-group">
        <span>Mã đơn hàng</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['madon']; ?>" >
        </div>
        <div class="form-group">
        <span>Tên khách hàng</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['tenkh']; ?>" >
        </div>
        <div class="form-group">
        <span>Nhân viên duyệt</span>
        <input type="text" readonly disabled class="form-control" value="<?= $_SESSION['manv']; ?>" >
        </div>
        <div class="form-group">
        <span>Nhân nhân giao</span>
        <?php
        $query2 = "SELECT * FROM nhanvien where quyen = 3";
        $result2 = $conn->query($query2);
        if(!$result2) echo 'Cau truy van bi sai';
        ?>
        <select name="nvgiao">
            <option value="NV04" selected="selected">--Chọn--</option>
            <?php while ($row2 = $result2->fetch_assoc()) { ?>
                <option value="<?= $row2['manv'] ?>" ><?= $row2['hotennv'] ?></option>
            <?php } ?>
        </select>
        </div>
        <div class="form-group">
        <span>Ngày đặt</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['ngaydat']; ?>" >
        </div>
        <div class="form-group">
        <span>Ngày giao</span>        
        <input type="date" name="ngaygiao" class="form-control" min="<?= $row['ngaydat']; ?>" />
        </div>
        <h2>Danh sách sản phẩm</h2>
        <table class="table table-hover" id="data-table">
        <?php

            $q = "SELECT * FROM chitietdonhang,sanpham where chitietdonhang.masp = sanpham.masp and madon = '$mdh'";
            $r = $conn->query($q);
            if(!$r) echo 'Cau truy van bi sai';
        ?>
        <thead>
        <tr bgcolor="#95f461">
            <th>Mã đơn hàng</th>
            <th>Tên sản phẩm</th>
            <th>Số Lượng SP</th>
            <th>Giá 1 sản phẩm</th>
        </tr>
        </thead>          
        <tbody>
        <?php $d=0;$t=0; while ($row3 = $r->fetch_assoc()) {$d++;
            $t += $row3['dh_giaban'] * $row3['dh_soluong'];
            if($d%2==1) $bg="#b0e5e5"; else $bg= "white";
            ?>
        <tr bgcolor="<?php echo $bg; ?>">
            <td><?= $row3['madon']; ?></td>
            <td><?= $row3['tensp']; ?></td>
            <td><?= $row3['dh_soluong']; ?></td>
            <td><?= $row3['dh_giaban']; ?></td>            
        </tr>
        <?php } ?>
        </tbody>
        </table>
        <div class="form-group">
        <span>Tổng số tiền cần thanh toán</span>        
        <input type="text" readonly disabled class="form-control" value="<?= $t; ?>" >
        </div>
        <div class="form-group">
        <input type="submit" name="DuyetDonHang" class="btn btn-primary btn-block" value="Duyệt đơn">            
        </div>
    </form>
    </div>
</div>