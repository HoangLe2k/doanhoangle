<!DOCTYPE html>
<!-- Designined by CodingLab - youtube.com/codinglabyt -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <title>Đăng ký thành viên!</title>
    <link rel="stylesheet" href="style.css">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
<body>
  <div class="container">
    <div class="title">Đăng ký thành viên mới!</div>
    <div class="content">
      <form action="action.php" method="POST">
        <div class="user-details">
          <div class="input-box">
            <span class="details">Họ và tên</span>
            <input type="text" name="hoten" placeholder="Nhập họ và tên" required>
          </div>
          <div class="gender-details">
          <input type="radio" name="gioitinh" value="1" id="dot-1" Checked>
          <input type="radio" name="gioitinh" value="0" id="dot-2">
          <span class="gender-title">Giới tính</span>
          <div class="category">
            <label for="dot-1">
            <span class="dot one"></span>
            <span class="gender">Nam</span>
          </label>
          <label for="dot-2">
            <span class="dot two"></span>
            <span class="gender">Nữ</span>
          </label>
          </div>
        </div>
          <div class="input-box">
            <span class="details">Email</span>
            <input type="text" name="email"  placeholder="Nhập email" required>
          </div>
          <div class="input-box">
            <span class="details">Số điện thoại</span>
            <input type="number" name="sdt" placeholder="Nhập số điện thoại" required>
          </div>
          <div class="input-box">
            <span class="details">Địa chỉ</span>
            <input type="text" name="diachi" placeholder="Nhập địa chỉ" required>
          </div>
          <div class="input-box">
            <span class="details">Tên đăng nhập</span>
            <input type="text" name="tk" placeholder="Tạo tên đăng nhập" required>
          </div>
          <div class="input-box">
            <span class="details">Mật khẩu</span>
            <input type="password" name="mk" placeholder="Tạo mật khẩu" required>
          </div>
          <div class="input-box">
            <span class="details">Nhập lại mật khẩu</span>
            <input type="password" name="remk" placeholder="Nhập lại mật khẩu" required>
          </div>
          <?php
          include('action.php');
            if(isset($_SESSION['thongbaoregister']))
            {
                echo '<div >
                <span class="details" style="color:red">**'.$_SESSION['thongbaoregister'].'</span>
                </div>';
                unset($_SESSION['thongbaoregister']);
            } ?>
        </div>
        <div class="button">
          <input type="submit" name="register" value="Đăng ký">
        </div>
      </form>
    </div>
  </div>

</body>
</html>
