
<div class="container">
    <?php include('action.php') ?>
    <div class="row">
    <h3  class="text-justify-center text-info">Nhập thông tin của sản phẩm mới</h3>
    <form weight=50% action="action.php" method="post" enctype="multipart/form-data">    
        <div class="form-group">
        <span>Tên sản phẩm</span>
        <input required type="text" name="tensp" class="form-control" placeholder="Nhập tên sản phẩm" >
        </div>
        <div class="form-group">
        <span>Đơn giá</span>
        <input required type="number" name="dongia" class="form-control" placeholder="Nhập đơn giá" >
        </div>
        <div class="form-group">
        <span>Mô tả sản phẩm</span>
        <input required type="text" name="mota" class="form-control" placeholder="Nhập mô tả sản phẩm" >
        </div>
        <div class="form-group">
        <span>Số lượng trong kho</span>
        <input required type="number" name="soluong" class="form-control" placeholder="Nhập số lượng" >
        </div>
        <div class="form-group">
        <span>Chọn nhóm sản phẩm</span>
        <?php
        $query2 = "SELECT * FROM nhomsp";
        $result2 = $conn->query($query2);
        if(!$result2) echo 'Cau truy van bi sai';
        ?>
        <select name="nhomsp">
            <option value="NSP01" selected="selected">--Chọn--</option>
            <?php while ($row2 = $result2->fetch_assoc()) { ?>
                <option value="<?= $row2['manhom'] ?>" ><?= $row2['tennhom'] ?></option>
            <?php } ?>
        </select>
        </div>
        <div class="form-group">
            <input type="file" name="hinh" class="custom-file">
        </div>
        <div class="form-group">
        <input type="submit" name="Addsp" class="btn btn-primary btn-block" value="Thêm sản phẩm">            
        </div>
    </form>
    </div>
</div>