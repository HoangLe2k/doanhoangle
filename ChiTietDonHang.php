
<div class="container">
    <?php include('action.php');
    $mdh = $_SESSION['madon'];
    $query = "SELECT * FROM donhang, khachhang where khachhang.makh = donhang.makh and donhang.madon = '$mdh' ";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $row = $result->fetch_assoc();
    
    ?>
    <div class="row">
    <h3  class="text-justify-center text-info">Chi tiết đơn hàng</h3>
    <form weight=50% action="action.php" method="post" >        
        <div class="form-group">
        <span>Mã đơn hàng</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['madon']; ?>" >
        </div>
        <div class="form-group">
        <span>Tên khách hàng</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['tenkh']; ?>" >
        </div>
        <div class="form-group">
        <span>Số điện thoại</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['sdt']; ?>" >
        </div>
        <div class="form-group">
        <span>Địa chỉ giao</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['diachi']; ?>" >
        </div>
        <?php
        $nvg= $row['nvgiao'];
        $nvd = $row['nvduyet'];
        if($nvd == "")
        {
            $Duyet = "";
        }
        else
        {
            $q2 = "SELECT * FROM donhang, nhanvien where '$nvd' =nhanvien.manv";
            $r2 = $conn->query($q2);
            if(!$r2) echo 'Cau truy van bi sai';
            $row2 = $r2->fetch_assoc();
            $Duyet = $row2['hotennv'];
        }
        if($nvg =="")
        {
            $Giao ="";
        }
        else
        {
            $q = "SELECT * FROM donhang, nhanvien where '$nvg' = nhanvien.manv";
            $r = $conn->query($q);
            if(!$r) echo 'Cau truy van bi sai';
            $row1 = $r->fetch_assoc();
            $Giao = $row1['hotennv'];
        }
        ?>
        <div class="form-group">
        <span>Nhân viên duyệt</span>
        <input type="text" readonly disabled class="form-control" value="<?= $Duyet; ?>" >
        </div>
        <div class="form-group">
        <span>Nhân viên giao</span>
        <input type="text" readonly disabled class="form-control" value="<?= $Giao; ?>" >
        </div>
        <div class="form-group">
        <span>Ngày đặt</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['ngaydat']; ?>" >
        </div>
        <div class="form-group">
        <span>Ngày giao</span>
        <input type="text" readonly disabled class="form-control" value="<?php if($row['ngaygiao'] == "0000-00-00") echo ""; else echo $row['ngaygiao'];?>" >
        </div>
        <h2>Danh sách sản phẩm</h2>
        <table class="table table-hover" id="data-table">
        <?php

            $q = "SELECT * FROM chitietdonhang,sanpham where chitietdonhang.masp = sanpham.masp and madon = '$mdh'";
            $r = $conn->query($q);
            if(!$r) echo 'Cau truy van bi sai';
        ?>
        <thead>
        <tr bgcolor="#95f461">
            <th>Mã đơn hàng</th>
            <th>Tên sản phẩm</th>
            <th>Số Lượng SP</th>
            <th>Giá 1 sản phẩm</th>
        </tr>
        </thead>          
        <tbody>
        <?php $d=0;$t=0; while ($row3 = $r->fetch_assoc()) {$d++;
            $t += $row3['dh_giaban'] * $row3['dh_soluong'];
            if($d%2==1) $bg="#b0e5e5"; else $bg= "white";
            ?>
        <tr bgcolor="<?php echo $bg; ?>">
            <td><?= $row3['madon']; ?></td>
            <td><?= $row3['tensp']; ?></td>
            <td><?= $row3['dh_soluong']; ?></td>
            <td><?= $row3['dh_giaban']; ?></td>            
        </tr>
        <?php } ?>
        </tbody>
        </table>
        <div class="form-group">
        <span>Tổng số tiền cần thanh toán</span>        
        <input type="text" readonly disabled class="form-control" value="<?= $t; ?>" >
        </div>
        <div class="form-group">
        <?php
        if($_SESSION['quyen'] == 3)
        {
            echo "<a href='action.php?dagiao=$mdh' class='btn btn-primary btn-block'>Đã giao</a>";
        }
        ?>
        
        <a style="background-color:red" href='adminHome.php?loadpage=QLDon.php' class="btn btn-primary btn-block">Quay về</a>           
        </div>
    </form>
    </div>
</div>