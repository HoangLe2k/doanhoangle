
<style>
        center{
            margin-top: 10px;
        }
    </style>
    <?php
        include 'config.php';
        if(!isset($_GET['page'])){
            $_GET['page'] = 1;
        }
        $rowPerPage = 2;
        // vị trí của mẩu tin đầu tiên trên mỗi trang
        $offset = ($_GET['page'] - 1) * $rowPerPage;

        $query = "SELECT * FROM sua LIMIT $offset, $rowPerPage";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi sai';

        // tổng số kết quả trả về
        $numRows = mysqli_num_rows($result);
        // tổng số trang hiển thị
        $maxPage = ceil($numRows / $rowPerPage);

    ?>

    <table style="border: 1px solid gray;text-align: justify;" align="center" cellspacing="0" width="600px">
        <?php
            if($result->num_rows != 0) {
                while($row = $result->fetch_array()) { ?>
                    <?php $hinh = $row['Hinh']; ?>
                    <tr>
                        <th bgcolor = #ff760033 colspan="2">
                            <h2 style="color: #ff5200;font-weight: bold;text-align: center;" class="text-center"><?= $row['Ten_sua']; ?></h2>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <img style="width: 100px;height: 100px;margin-left: auto;" src="./SQL_PHP/images/<?= $hinh ?>" alt="">
                        </td>
                        <td>
                            <p><b>Thành phần dinh dưỡng:</b></p>
                            <p><?= $row['TP_Dinh_Duong'] ?></p>
                            <p><b>Lợi ích:</b></p>
                            <p><?= $row['Loi_ich'] ?></p>
                            <span class="text-right"> 
                                <b>Trọng lương: </b><?= $row['Trong_luong'].' gr'; ?> -
                                <b>Đơn giá: </b><?= $row['Don_gia'].' VNĐ'; ?>
                            </span>
                        </td>
                    </tr>
                <?php } 
            }
        ?> 
    </table>
    <center>
        <?php 
            $re = $conn->query('SELECT * FROM sua');
            $numRows = mysqli_num_rows($re);
            $maxPage = ceil($numRows/$rowPerPage);
            if ($_GET['page'] > 1){
                echo '<a style="text-decoration: none;" href="./SQL_PHP/Bai2.8/hienThi.php?page='.($_GET['page'] - 1).'"> << </a>'; //gắn thêm nút Back
            }
            for ($i=1 ; $i<=$maxPage ; $i++)
            {
                if($i == $_GET['page'])
                {
                    echo '<b> <u>'.$i.'</u> </b>'; //trang hiện tại sẽ được bôi đậm
                }
                else echo '<a style="text-decoration: none;" href="./SQL_PHP/Bai2.8/hienThi.php?page='.$i.'"> '.$i.' </a>';
            }
            if ($_GET['page'] < $maxPage) {
                echo '<a style="text-decoration: none;" href="./SQL_PHP/Bai2.8/hienThi.php?page='.($_GET['page'] + 1).'"> >> </a>';  //gắn thêm nút Next
            }
            $conn->close();
        ?>
    </center>
