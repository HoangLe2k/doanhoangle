<div class="container">
<form>
    <?php
        include('action.php');

        $query = "SELECT * FROM donhang, khachhang where khachhang.makh = donhang.makh and donhang.tinhtrang = '1' ORDER BY donhang.ngaydat DESC";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi sai';
    ?>
    <h3 class="text-center text-info">Danh sách đơn hàng</h3>
    <?php if(isset($_SESSION['thongbaoQLdon_chuaduyet']))
        {
            echo '<div>
            <span style="color:red">'.$_SESSION['thongbaoQLdon_chuaduyet'].'</span>
            </div>';
            unset($_SESSION['thongbaoQLdon_chuaduyet']);
        } ?>
    <table class="table table-hover" id="data-table">
        <thead>
        <tr bgcolor="#95f461">
            <th>Mã đơn hàng</th>
            <th>Họ tên khách hàng</th>
            <th>Nhân viên giao</th>
            <th>Nhân viên duyệt</th>
            <th>Ngày đặt</th>
            <th>Ngày giao</th>
            <th>Tình trạng</th>
            <th></th>
        </tr>
        </thead>          
        <tbody>
        <?php $d=0; while ($row = $result->fetch_assoc()) {$d++;
            if($d%2==1) $bg="#b0e5e5"; else $bg= "white";

            switch($row['tinhtrang'])
            {
                case "1": // đã giao
                    $tt = "Đang duyệt";
                    break;
                case "2": // đang duyệt
                    $tt = "đang giao";
                    break;
                case "3": // đang giao
                    $tt = "Đã hủy";
                    break;
                case "4": // đã hủy
                    $tt = "Đã giao";
                    break;
            }
            ?>
        <tr bgcolor="<?php echo $bg; ?>">
            <td><?= $row['madon']; ?></td>
            <td><?= $row['tenkh']; ?></td>
            <td></td>
            <td></td>
            <td><?= $row['ngaydat']; ?></td>
            <td></td>
            <td><?= $tt; ?></td>
            <td><a href="action.php?duyet=<?= $row['madon'];?>" class="badge badge-primary p-2">Duyệt đơn</a></td>            
        </tr>
        <?php } ?>
        </tbody>
    </table>
</form>
</div>