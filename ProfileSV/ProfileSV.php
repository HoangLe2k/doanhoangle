
<!-- 
Magazee Template 
http://www.templatemo.com/tm-514-magazee
-->
  <!-- load CSS -->
  



<body>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">    <!-- Google web font "Open Sans" -->
  <link rel="stylesheet" href="./ProfileSV/css/bootstrap.min.css">                                        <!-- https://getbootstrap.com/ -->
  <link rel="stylesheet" href="./ProfileSV/css/templatemo-style.css">                                     <!-- Templatemo style -->

  <script>
    var renderPage = true;

    if(navigator.userAgent.indexOf('MSIE')!==-1
      || navigator.appVersion.indexOf('Trident/') > 0){
        /* Microsoft Internet Explorer detected in. */
        alert("Please view this in a modern browser such as Chrome or Microsoft Edge.");
        renderPage = false;
    }
  </script>
  <!-- Loader -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>

 <div class="container">

  <!-- 1st section -->
  <section class="row tm-section">
   <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 p-0">
    <div class="tm-flex-center p-5 tm-bg-color-primary">
      <div class="tm-max-w-400 tm-flex-center tm-flex-col">
        <img src="./ProfileSV/img/image-04.png" alt="Image" class="rounded-circle mb-4">
        <h5 class="tm-text-color-white tm-site-name">LÊ NGUYỄN VIỆT HOÀNG</h5>
      </div>
  </div>
  </div>

  <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
  <div class="tm-flex-center p-5">
      <div class="tm-md-flex-center">
        <h2 class="tm-text-color-primary mb-4">MSSV: 60131564</h2>        
        <h5 class="mb-4">Chuyên ngành:Công nghệ thông tin(CNPM)</h5>
        <h5 class="mb-4">Khoa: Công nghệ thông tin - trường ĐH Nha Trang</h5>
        <h5 class="mb-4">Ngày sinh: 02-01-2000</h5>
        <h5 class="mb-4">Quê quán: Ninh Hòa - Khánh Hòa</h5>
        <p class="mb-4">Định hướng nghề nghiệp: Trở thành một lập trình viên chuyên nghiên cứu về xử lý thuật toán, làm việc với mảng back-end, lập trình ứng dụng di động...</p>
        
      </div>
    </div>
  </div>

  
</section>

<!-- 2nd section -->
<section class="row tm-section tm-col-md-reverse">
  <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
      <div class="tm-flex-center p-5">
        <q class="tm-quote tm-text-color-gray">Học hỏi và không ngừng cố gắng, không ngừng cải thiện bản thân !</q>
      </div>
  </div>
<div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 p-0">
    <div  class="tm-flex-center p-5 tm-bg-color-primary tm-section-min-h">
        <h2 class="tm-text-color-white tm-site-name">lớp: 60CNTT - 2</h2>
    </div>
</div>
</section>

<!-- 3rd Section -->
<section class="row tm-section tm-mb-30">
  <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 p-0 text-center">
    <img src="./ProfileSV/img/image-01.jpg" alt="Image" class="img-fluid">
  </div>
  <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
    <div class="tm-flex-center p-5">
      <div class="tm-flex-center tm-flex-col">
        <h2 class="tm-align-left">Sở thích</h2>
        <p>Thích được ăn cơm cùng với gia đình, người yêu. Thích chơi game, xem phim kinh dị, tám chuyện cùng với bạn bè. Thích chụp ảnh, chụp mọi thứ trong tầm mắt !</p>
      </div>
    </div>
  </div>
</section>

<!-- 4th Section -->
<section class="row tm-section tm-mb-30">
 <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8">
  <div class="tm-flex-center pl-5 pr-5 pt-5 pb-5">
    <div class="tm-md-flex-center">
     <h2 class="mb-4 tm-text-color-primary">Thu hoạch(HP:Phát triển ứng dụng mã nguồn mở)</h2>
     <p>Hp: Phát triển ứng dụng mã nguồn mở do Thầy Nguyễn Hải Triều hướng dẫn. Là một môn học về cách để xây dựng và phát triển một ứng dụng/website mã nguồn mở. Thầy sử dụng ngôn ngữ PHP và hệ QTCSDL mySQL để hướng dẩn xây dựng website.</p>
     <p class="mb-4">Qua những bài giảng, và những buổi thực hành em có thể hiểu hơn về cách để xây dựng một website cũng như CSDL trên nền tảng giả lập Xampp. Các bài giảng của thầy rất chi tiết và dễ hình dung, dễ tiếp thu.</p>
     <p class="mb-4">Mặc dù học và thi trực tuyến, nhưng em có thể tự tin thu hoạch được hơn 90% các bài giảng của thầy! Đây sẽ là nền tảng vững chắc để em cũng như các bạn sinh viên khác sẽ dễ dàng thích nghi với môi trường là việc sau này.</p>
   </div>
 </div>
</div>
<div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 text-xl-right text-md-center text-center mt-5 mt-lg-0 pr-lg-0">
 <img src="./ProfileSV/img/image-02.jpg" alt="Image" class="img-fluid">
</div>
</section>

<!-- 5th Section -->

<!-- Footer -->
<div class="row">
  <div class="col-lg-12">
    <p class="text-center small tm-copyright-text mb-0">Copyright &copy; <span class="tm-current-year">2018</span> Your Company Name | Designed by Template Mo</p>
  </div>
</div>
</div>
<!-- load JS -->
<script src="./ProfileSV/js/jquery-3.2.1.slim.min.js"></script>         <!-- https://jquery.com/ -->
<script>

  /* DOM is ready
  ------------------------------------------------*/
  $(function(){

    if(renderPage) {
      $('body').addClass('loaded');
    }

    $('.tm-current-year').text(new Date().getFullYear());  // Update year in copyright
  });

</script>

</body>
</html>