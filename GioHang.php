<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Giỏ hàng</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
    
<!-- header section starts      -->
<?php
include('action.php');
?>
<header>

    <a href="#" class="logo"><img width="100px" height="20px" src="images/logo.png"></img></a>

    <nav class="navbar">
        <a class="active" href="index.php#home">Trang Chủ</a>
        <a href="index.php#dishes">Món Ăn</a>
        <a href="index.php#about">Thông Tin</a>
        <a href="index.php#review">Đánh Giá</a>
        
    </nav>

    <div class="icons">
        <i class="fas fa-bars" id="menu-bars"></i>
        <?php
            if(isset($_SESSION['makh']))
            {
                echo "<span style='font-size: 20px;' color='#27ae60'>".$_SESSION['tenkh']."</span>";
                echo '<a href="#" class="fas fa-user-alt"></a>';
                echo '<a href="action.php?logout" class="fas fa-sign-out-alt"></a>';
            }
            else
            {
                echo '<a href="login.php" class="fas fa-user-alt"></a>';
            }

        ?>   
        
        
    </div>

</header>

<!-- header section ends-->

<section style="padding-top: 100px; padding-bottom: 100px;" class="body">
<center>
    <form method="POST" action="action.php">
        <table class="table table-hover" id="data-table">
            <tr>
                <th colspan=5><center><h1>Giỏ hàng của <?= $_SESSION['tenkh']; ?></h1></center></th>
            </tr>
            <tr bgcolor="#95f461">
                <td><h2>Hình ảnh sản phẩm</h2></td>
                <td><h2>Tên sản phẩm</h2></td>
                <td><h2>Đơn giá</h2></td>
                <td><h2>Số lượng</h2></td>
                <td><h2>Hành động</h2></td>             
            </tr>
            <?php
                $mkh = $_SESSION['makh'];
                $query = "SELECT* FROM giohang,sanpham WHERE giohang.masp = sanpham.masp and makh = '$mkh'";
                $result = $conn->query($query);
                if(!$result) echo 'Cau truy van bi sai';
                $q = "SELECT* FROM khachhang WHERE makh = '$mkh'";
                $r = $conn->query($q);
                if(!$r) echo 'Cau truy van bi sai';
                $row2 = $r->fetch_assoc();
                $t = 0;
                while ($row = $result->fetch_assoc()) {$msp = $row['masp']; $t += $row['dongia']*$row['soluong_gh']; ?>
                    <tr>
                    <td><img style="width: 100px;" src="./images/<?= $row['hinh']; ?>" ></td>
                    <td><h2><?= $row['tensp']; ?></h2></td>
                    <td><h2><?= $row['dongia']; ?></h2></td>
                    <td><a href="action.php?botSPGH=<?= $msp ?>" style="background:#2f2fbc" class='btn'>-</a><input style="font-size: 20px;width: 50px; padding-bottom: 15px;" disabled type="text" value="<?= $row['soluong_gh'] ?>"><a href="action.php?themSPGH=<?= $msp ?>" style="background:#2f2fbc" class='btn'>+</a></td>
                    <td><a style='background-color: #fc3232;' href="action.php?xoaSPGH=<?= $msp ?>" class='btn badge badge-primary p-2'>Xóa</a></td>
                    </tr>            
            <?php } ?>
            
        </table>
        <div class="form-group">
        <h2 style="color:red">địa chỉ giao hàng:</h2>
        <input style="font-size: 20px;width: 650px; padding-bottom: 15px;" name="diachigiao" type="text" value="<?= $row2['diachi'] ?>">
        </div>
        <div class="form-group">
        <h2 style="color:red">Tổng số tiền cần thanh toán: <?= $t; ?></h2>
        </div>
        <div class="form-group">
        <input type="submit" name="Okdathang" class="btn btn-primary btn-block" value="Đặt hàng">            
        </div>
    </form>
</center>

</section>


<!-- footer section starts  -->

<section class="footer">

    <div class="box-container">

        <div class="box">
            <h3>locations</h3>
            <a href="#">Trường Đại học Nha Trang</a>
            <a href="#">Khoa Công nghệ thông tin</a>
            <a href="#">Môn phát triển UD mã nguồn mở</a>
        </div>

        <div class="box">
            <h3>quick links</h3>
            <a href="index.php#home">Trang Chủ</a>
            <a href="index.php#dishes">Món Ăn</a>
            <a href="index.php#about">Thông Tin</a>
            <a href="index.php#review">Đánh Giá</a>
        </div>

        <div class="box">
            <h3>Author info</h3>
            <a href="#">SV: Lê Nguyễn Việt Hoàng</a>
            <a href="#">MSSV: 60131564</a>
            <a href="#">email:hoang.lnv.60cntt@ntu.edu.vn</a>
            <a href="#">Lớp:60cntt-2</a>
        </div>

        <div class="box">
            <h3>follow me</h3>
            <a href="#">facebook</a>
            <a href="#">twitter</a>
            <a href="#">instagram</a>
            <a href="#">linkedin</a>
        </div>

    </div>

    <div class="credit">Template gốc từ copyright @ 2021 by <span>mr. web designer</span> </div>

</section>

<!-- footer section ends -->

<!-- loader part  -->






















<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<!-- custom js file link  -->
<script src="js/script.js"></script> 

</body>
</html>