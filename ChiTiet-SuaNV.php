
<div class="container">
    <?php include('action.php') ?>
    <div class="row">
    <h3  class="text-justify-center text-info">Thông tin của nhân viên</h3>
    <?php
    $Mnv = $_SESSION['mnv'];
    $query = "SELECT * FROM nhanvien where manv = '$Mnv'";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $row = $result->fetch_assoc();
    ?>
    <form action="action.php" method="post" >
        <div class="form-group">
        <span>Mã nhân viên</span>
        <input type="text" readonly disabled class="form-control" value="<?= $row['manv'] ?> ">
        </div>    
        <div class="form-group">
        <span>Họ tên nhân viên</span>
        <input type="text" name="hotennv" class="form-control" value="<?= $row['hotennv'] ?> ">
        </div>
        <div class="form-group">
        <span>Giới tính</span>
        Nam<input type="radio" name="gioitinh" <?php if($row['gioitinh']==1) echo "checked" ?> value="1">&nbsp;Nữ<input type="radio" name="gioitinh" <?php if($row['gioitinh']==0) echo "checked" ?> value="0">
        </div>        
        <div class="form-group">
        <span>Email</span>
        <input type="text" name="email" class="form-control" value="<?= $row['email_nv'] ?>">
        </div>
        <div class="form-group">
        <span>Số điện thoại</span>
        <input type="text" name="sdt" class="form-control" value="<?= $row['sdt'] ?>">
        </div>
        <div class="form-group">
        <h5>Quyền nhân viên</h5>
        Quản trị viên<input type="radio" name="quyen" <?php if($row['quyen']==1) echo "checked" ?> value="1">&nbsp;Nhân viên CSKH<input type="radio" name="quyen" <?php if($row['quyen']==2) echo "checked" ?> value="2">&nbsp;Nhân viên giao hàng<input type="radio" name="quyen" <?php if($row['quyen']==3) echo "checked" ?> value="3">
        </div>
        <div class="form-group">
        <span>Tên đăng nhập</span>
        <input type="text" name="tendangnhap" class="form-control" value="<?= $row['tendangnhap'] ?>">
        </div>
        <div class="form-group">
        <span>Mật Khẩu</span>
        <input type="text" name="matkhau" class="form-control" value="<?= $row['matkhau'] ?>" >
        </div>
        <div class="form-group">
        <a href='adminHome.php?loadpage=QLnhanvien.php' class='badge badge-primary p-2'>Quay về</a>
        <input type="submit" name="sua" style='background-color: #6be56d;' value="Lưu thay đổi">        
        <a style='background-color: #fc3232;' href='action.php?xoa=<?=$Mnv;?>' class='badge badge-primary p-2'>Xóa NV</a>              
        </div>
    </form>
    </div>
</div>