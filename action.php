<?php
$key = "HoangLe";
include('conf.php');
if (session_id() === '') session_start();
if(isset($_GET['goclient']))
{
    session_destroy();
    header ("location:index.php");
}
if(isset($_GET['goGioHang']))
{
    header ("location:GioHang.php");
}
if(isset($_GET['ProfileNV']))
{
    $_SESSION['tieude'] = "Thông tin nhân viên";
    header ("location:ShiperHome.php?loadpage=ProfileNV.php");
}
if(isset($_GET['goBaitap']))
{
    $_SESSION['tieude'] = "Bài tập thực hành của SV";
    header ("location:adminHome.php?loadpage=baitappage.php");
}
if(isset($_GET['goProfileSV']))
{
    $_SESSION['tieude'] = "Thông tin sinh viên";
    header ("location:adminHome.php?loadpage=./ProfileSV/ProfileSV.php");
}
if(isset($_POST['goQlKH']))
{
    $_SESSION['tieude'] = "Quản lý khách hàng";
    header ("location:adminHome.php?loadpage=QLkhachhang.php");
}
if(isset($_POST['goQlNV']))
{
    $_SESSION['tieude'] = "Quản lý nhân viên";
    header ("location:adminHome.php?loadpage=QLnhanvien.php");
}
if(isset($_POST['goAddNV']))
{
    if($_SESSION['quyen']==1)
    {
        $_SESSION['tieude'] = "Thêm Nhân viên";
        header ("location:adminHome.php?loadpage=AddNhanvien.php");
    }
    
    else header ("location:adminHome.php?loadpage=-1");
}
if(isset($_POST['goQlDon']))
{
    $_SESSION['tieude'] = "Quản lý đơn hàng";
    header ("location:adminHome.php?loadpage=QlDon.php");
} 
if(isset($_POST['goDuyetDon']))
{
    if($_SESSION['quyen']==3) header ("location:adminHome.php?loadpage=-1");    
    else
    {
        $_SESSION['tieude'] = "Quản lý đơn hàng đang chờ duyệt";
        header ("location:adminHome.php?loadpage=QlDon_ChuaDuyet.php");
    }
}
if(isset($_POST['goQlSanpham']))
{
    $_SESSION['tieude'] = "Quản lý sản phẩm";
    header ("location:adminHome.php?loadpage=QlSanpham.php");
}
if(isset($_POST['goAddNhomsp']))
{
    $_SESSION['tieude'] = "Thêm nhóm sản phẩm mới";
    header ("location:adminHome.php?loadpage=AddNSP.php");
}
if(isset($_POST['goAddSanpham']))
{
    $_SESSION['tieude'] = "Thêm sản phẩm mới";
    header ("location:adminHome.php?loadpage=AddSanpham.php");
}

if(isset($_POST['goBaitap']))
{
    switch($_POST['baitap'])
    {
        case "":
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Arb1":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTArray/Bai1.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Arb2":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTArray/Bai2.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Arb3":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTArray/Bai3.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Arb4":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTArray/Bai4.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Arb5":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTArray/Bai5.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Arb6":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTArray/Bai6.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Arb7":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTArray/Bai7.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Fb1":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTForm/Bai1.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Fb2":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTForm/Bai2.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Fb3":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTForm/Bai3.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Fb4":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTForm/Bai4.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Fb5":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTForm/Bai5.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "Fb6":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./BTForm/Bai6.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql1":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.1/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql2":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.2/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql3":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.3/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql4":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.4/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql5":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.5/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql6":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.6/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql8":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.8/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
        case "sql9":            
            unset($_SESSION['baitap']);
            $_SESSION['baitap'] = "./SQL_PHP/Bai2.9/hienThi.php";
            header ("location:adminHome.php?loadpage=baitappage.php");
            break;
    }
}
if(isset($_POST['timkiemQLSP']))
{
    $_SESSION['timkiemQLSP'] = $_POST['timQLSP'];
    header ("location:adminHome.php?loadpage=QlSanpham.php");
}
if(isset($_POST['timkiemQLD']))
{
    $_SESSION['timkiemQLD'] = $_POST['timQLD'];
    header ("location:adminHome.php?loadpage=QlDon.php");
}
if(isset($_POST['timkiemQLNV']))
{
    $_SESSION['timkiemQLNV'] = $_POST['timQLNV'];
    header ("location:adminHome.php?loadpage=Qlnhanvien.php");
}
if(isset($_POST['timkiemQLKH']))
{
    $_SESSION['timkiemQLKH'] = $_POST['timQLKH'];
    header ("location:adminHome.php?loadpage=QLkhachhang.php");
}
if(isset($_POST['DoiPass']))
{
    
    $pas = $_POST['newpass'];
    $repas = $_POST['renewpass'];
    if($pas == $repas)
    {
        $mkh = $_SESSION['makh'];
        $pas = md5($pas . $key);
        $q = "UPDATE khachhang SET matkhau = '$pas' WHERE makh = '$mkh'";
        $result = $conn->query($q);
        if($result) header ("location:ProfileKH.php?done");
    }
    else
    {
        header ("location:doipass.php?error");
    }
}
if(isset($_POST['DoiPassNV']))
{
    
    $pas = $_POST['newpass'];
    $repas = $_POST['renewpass'];
    if($pas == $repas)
    {
        $mnv = $_SESSION['manv'];
        $pas = md5($pas . $key);
        $q = "UPDATE nhanvien SET matkhau = '$pas' WHERE manv = '$mnv'";
        $result = $conn->query($q);
        if($result)
        {
            $_SESSION['tieude'] = "Thông tin nhân viên";
            $_SESSION['done'] = 1;
            header ("location:ShiperHome.php?loadpage=ProfileNV.php");
        }
    }
    else
    {
        $_SESSION['error'] = 1;
        header ("location:ShiperHome.php?loadpage=doipassNV.php");
    }
}
if(isset($_POST['Okdathang']))
{
    $mkh = $_SESSION['makh'];
    
    $q1 = "SELECT* FROM giohang,sanpham WHERE giohang.masp = sanpham.masp and makh = '$mkh'";
    $r1 = $conn->query($q1);
    if(!$r1) echo 'Cau truy van bi sai';
    if($r1->num_rows ==0)
    {
        header ("location:index.php?error");
    }
    else
    {
        require_once('Functions.php');
        $Madon = autoID("dh");
        $ngaydat = date("Y-m-d");
        $tt= "1";
        $query = "INSERT INTO donhang(madon,makh,nvgiao,nvduyet,ngaydat,ngaygiao,tinhtrang)VALUES('$Madon','$mkh','','','$ngaydat','','$tt')";
            if($conn->query($query)==true)
            {
                $query2 = "SELECT* FROM giohang,sanpham WHERE giohang.masp = sanpham.masp and makh = '$mkh'";
                    $result2 = $conn->query($query2);
                    if(!$result2) echo 'Cau truy van bi sai';
                    while ($row = $result2->fetch_assoc())
                    {
                        $masp = $row['masp'];
                        $dh_sl = $row['soluong_gh'];
                        $gia = $row['dongia'];

                        $q3 = "SELECT* FROM sanpham WHERE masp = '$masp'";
                        $r3 = $conn->query($q3);
                        
                        $row3 = $r3->fetch_assoc();
                        $sl = $row3['soluong'] - $dh_sl;
                        $q4 = "UPDATE sanpham SET soluong = '$sl' WHERE masp = '$masp'";
                        $r4 = $conn->query($q4);

                        $q = "INSERT INTO chitietdonhang(madon,masp,dh_soluong,dh_giaban)VALUES('$Madon','$masp','$dh_sl','$gia')";
                        if($conn->query($q)==false) echo "thêm bị lỗi";                           
                    }
                    $q2 = "DELETE FROM giohang WHERE makh = '$mkh'";
                    $r2 = $conn->query($q2);
                    
                    $diachi = $_POST['diachigiao'];
                    $q5 = "UPDATE khachhang SET diachi = '$diachi' WHERE makh = '$mkh'";
                    $r5 = $conn->query($q5);
                    if(!$r5) echo 'Cau truy van bi sai';

                    if(!$r2) echo 'Cau truy van bi sai';
                    else
                    {                    
                        header ("location:index.php?cd");
                    }
            }
            else echo "thất bại";
    }
    
}
if(isset($_GET['sp']))
{
    if(isset($_SESSION['makh']) && $_SESSION['makh'] != "")
    {
        $msp = $_GET['sp'];
        $mkh = $_SESSION['makh'];

        $query = "SELECT* FROM giohang WHERE makh = '$mkh' and masp = '$msp'";
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi sai';

        if($result->num_rows == 0)
        {
            $q = "INSERT INTO giohang(makh,masp,soluong_gh)VALUES('$mkh','$msp',1)";
            if($conn->query($q)==true)
            {
                header ("location:index.php");
            }
            else echo "thất bại";
        }
        else
        {
            $row = $result->fetch_assoc();
            $sl = $row['soluong_gh'] + 1;
            $q = "UPDATE giohang SET soluong_gh = '$sl' WHERE makh = '$mkh' and masp = '$msp'";
            $r = $conn->query($q);
            if(!$r) echo 'Cau truy van bi sai';
            else
            {
                header ("location:index.php");
            }
        }
    }
    else header ("location:login.php");    
}


if(isset($_POST['Addsp']))
{
    require_once('Functions.php');
    $Masp = autoID("sp");
    $Tensp = $_POST['tensp'];
    $Dongia = $_POST['dongia'];
    $Mota = $_POST['mota'];
    $Soluong = $_POST['soluong'];
    $Nhomsp = $_POST['nhomsp'];
    $Hinh = $_FILES['hinh']['name'];

    $query = "INSERT INTO sanpham(masp,tensp,dongia,mota,soluong,hinh,manhom)VALUES('$Masp','$Tensp','$Dongia','$Mota','$Soluong','$Hinh','$Nhomsp')";
        if($conn->query($query)==true)
        {
            $_SESSION['thongbaoAddSP'] = "Thêm sản phẩm mới thành công!";
            $path = "images/".$Hinh;
            move_uploaded_file($_FILES['hinh']['tmp_name'], $path);
            header ("location:adminHome.php?loadpage=QlSanpham.php");
        }
        else "thất bại";
}
if(isset($_POST['AddNSP']))
{

    require_once('Functions.php');
    $Manhom = autoID("nsp");
    $Tennhom = $_POST['tennhom'];
    $query = "INSERT INTO nhomsp(manhom,tennhom)VALUES('$Manhom','$Tennhom')";
        if($conn->query($query)==true)
        {
            $_SESSION['thongbaoAddNSP'] = "Thêm nhóm sản phẩm mới thành công!";
            header ("location:adminHome.php?loadpage=QlSanpham.php");
        }
        else "thất bại";
}
if(isset($_GET['duyet']))
{
    $_SESSION['mdh'] = $_GET['duyet'];
    $_SESSION['tieude'] = "Duyệt đơn hàng";
    header ("location:adminHome.php?loadpage=DuyetDon.php");
}
if(isset($_GET['chitiet']))
{
    $_SESSION['mnv'] = $_GET['chitiet'];   
    $_SESSION['tieude'] = "Xem/Sửa thông tin nhân viên";
    header ("location:adminHome.php?loadpage=ChiTiet-SuaNV.php");
}
if(isset($_GET['chitietKH']))
{
    $_SESSION['mkh'] = $_GET['chitietKH'];   
    $_SESSION['tieude'] = "Xem/Sửa thông tin khách hàng";
    header ("location:adminHome.php?loadpage=ChiTiet-SuaKH.php");
}
if(isset($_GET['chitietSP']))
{
    $_SESSION['masp'] = $_GET['chitietSP'];
    $_SESSION['tieude'] = "Xem/Sửa thông tin sản phẩm";
    header ("location:adminHome.php?loadpage=ChiTiet-SuaSP.php");
}
if(isset($_GET['chitietDon']))
{
    $_SESSION['madon'] = $_GET['chitietDon'];
    $_SESSION['tieude'] = "Xem thông tin đơn hàng";
    header ("location:adminHome.php?loadpage=ChiTietDonHang.php");
}
if(isset($_GET['chitietDonship']))
{
    $_SESSION['madon'] = $_GET['chitietDonship'];
    $_SESSION['tieude'] = "Thông tin đơn hàng cần giao";
    header ("location:ShiperHome.php?loadpage=ChiTietDonHang.php");
}
if(isset($_GET['passNV']))
{    
    $_SESSION['tieude'] = "Đổi mật khẩu";
    header ("location:ShiperHome.php?loadpage=doipassNV.php");
}

if(isset($_GET['dagiao']))
{
    $madon = $_GET['dagiao'];
    $query = "UPDATE donhang SET tinhtrang ='4' WHERE madon = '$madon'";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    else
    {    
        header ("location:ShiperHome.php");        
    }
}

if(isset($_GET['xoa']))
{
    $Mnv = $_GET['xoa'];
    $q = "DELETE FROM nhanvien WHERE manv = '$Mnv'";
    $r = $conn->query($q);
    if(!$r) echo 'Cau truy van bi sai';
    else
    {
        $_SESSION['thongbaoQlNV'] = "Xóa nhân viên Thành Công!";
        $_SESSION['tieude'] = "Quản lý nhân viên";
        unset($_SESSION['mnv']);
        header ("location:adminHome.php?loadpage=QLnhanvien.php");
    }
}
if(isset($_GET['xoaKH']))
{
    $Mkh = $_GET['xoaKH'];
    $q = "DELETE FROM khachhang WHERE makh = '$Mkh'";
    $r = $conn->query($q);
    if(!$r) echo 'Cau truy van bi sai';
    else
    {
        $_SESSION['thongbaoQlKH'] = "Xóa khách hàng Thành Công!";
        $_SESSION['tieude'] = "Quản lý khách hàng";
        header ("location:adminHome.php?loadpage=QLkhachhang.php");
    }
}
if(isset($_GET['xoaNSP']))
{
    
    $Mnsp = $_GET['xoaNSP'];
    $q = "DELETE FROM nhomsp WHERE manhom = '$Mnsp'";
    $r = $conn->query($q);
    if(!$r) echo 'Cau truy van bi sai';
    else
    {
        $_SESSION['thongbaoAddNSP'] = "Xóa Nhóm SP Thành Công!";
        $_SESSION['tieude'] = "Quản lý sản phẩm";
        header ("location:adminHome.php?loadpage=QLSanpham.php");
    }
}
if(isset($_GET['xoaSP']))
{
    
    $Msp = $_GET['xoaSP'];
    $q = "DELETE FROM sanpham WHERE masp = '$Msp'";
    $r = $conn->query($q);
    if(!$r) echo 'Cau truy van bi sai';
    else
    {
        $_SESSION['thongbaoAddSP'] = "Xóa SP Thành Công!";
        $_SESSION['tieude'] = "Quản lý sản phẩm";
        header ("location:adminHome.php?loadpage=QLSanpham.php");
    }
}
if(isset($_GET['botSPGH']))
{
    
    $mkh = $_SESSION['makh'];
    $msp = $_GET['botSPGH'];
    
    $query = "SELECT* FROM giohang WHERE makh = '$mkh' and masp = '$msp'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $sl = $row['soluong_gh'] - 1;

    if($sl <= 0)
    {
        header ("location:GioHang.php");        
    }
    else
    {
        $q = "UPDATE giohang SET soluong_gh = '$sl' WHERE makh = '$mkh' and masp = '$msp'";
        $r = $conn->query($q);
        if(!$r) echo 'Cau truy van bi sai';
        else
        {
            header ("location:GioHang.php");
        } 
    }  
}
if(isset($_GET['themSPGH']))
{
    
    $mkh = $_SESSION['makh'];
    $msp = $_GET['themSPGH'];
    
    $q2 = "SELECT* FROM sanpham WHERE masp = '$msp'";
    $r2 = $conn->query($q2);
    $row2 = $r2->fetch_assoc();
    $slmax = $row2['soluong'];

    $query = "SELECT* FROM giohang WHERE makh = '$mkh' and masp = '$msp'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $sl = $row['soluong_gh'] + 1;
    if($sl > $slmax)
    {
        header ("location:GioHang.php");
    }
    else
    {
        $q = "UPDATE giohang SET soluong_gh = '$sl' WHERE makh = '$mkh' and masp = '$msp'";
        $r = $conn->query($q);
        if(!$r) echo 'Cau truy van bi sai';
        else
        {
            header ("location:GioHang.php");
        } 
    }
      
}
if(isset($_GET['xoaSPGH']))
{
    
    $mkh = $_SESSION['makh'];
    $msp = $_GET['xoaSPGH'];
    $q = "DELETE FROM giohang WHERE masp = '$msp' and makh = '$mkh'";
    $r = $conn->query($q);
    if(!$r) echo 'Cau truy van bi sai';
    else
    {
        header ("location:GioHang.php");
    }    
}
if(isset($_POST['sua']))
{
    $mnv = $_SESSION['mnv'];
    $ten = $_POST['hotennv'];
    $GT = $_POST['gioitinh'];
    $Email = $_POST['email'];
    $Sdt = $_POST['sdt'];
    $Quyen = $_POST['quyen'];
    $Tk = $_POST['tendangnhap'];
    $Mk = md5($_POST['matkhau'] . $key);
    $q = "UPDATE nhanvien SET hotennv = '$ten', gioitinh = '$GT', email_nv = '$Email', sdt = '$Sdt', quyen = '$Quyen', tendangnhap = '$Tk', matkhau = '$Mk' WHERE manv = '$mnv'";
    $r = $conn->query($q);
    if(!$r) echo 'Cau truy van bi sai';
    else
    {
        $_SESSION['thongbaoQlNV'] = "Thay đổi Thông tin NV Thành Công!";
        $_SESSION['tieude'] = "Quản lý nhân viên";
        unset($_SESSION['mnv']);
        header ("location:adminHome.php?loadpage=QLnhanvien.php");
        
    }    
}
if(isset($_POST['suaKH']))
{
    $mkh = $_SESSION['mkh'];
    $ten = $_POST['tenkh'];
    $GT = $_POST['gioitinh'];
    $Email = $_POST['email'];
    $Sdt = $_POST['sdt'];
    $Diachi = $_POST['diachi'];
    $Tk = $_POST['tendangnhap'];
    $Mk = md5($_POST['matkhau'] . $key);
    $q = "UPDATE khachhang SET tenkh = '$ten', gioitinh = '$GT', email_kh = '$Email', sdt = '$Sdt', diachi = '$Diachi', tendangnhap = '$Tk', matkhau = '$Mk' WHERE makh = '$mkh'";
    $r = $conn->query($q);
    if(!$r) echo 'Cau truy van bi sai';
    else
    {
        $_SESSION['thongbaoQlKH'] = "Thay đổi Thông tin KH Thành Công!";
        $_SESSION['tieude'] = "Quản lý khách hàng";
        unset($_SESSION['mkh']);
        header ("location:adminHome.php?loadpage=QLkhachhang.php");
        
    }    
}
if(isset($_POST['suaSP']))
{
    $Masp = $_SESSION['masp'];
    $Ten = $_POST['tensp'];
    $Dg = $_POST['dongia'];
    $Mt = $_POST['mota'];
    $Sl = $_POST['soluong'];
    $Nhom = $_POST['nhomsp'];
    $Anhc = $_POST['hinhc'];
    if(isset($_FILES['hinh']['name'])&&($_FILES['hinh']['name']!=""))
    {
        $Anh = $_FILES['hinh']['name'];
        $path = "images/".$Anh;
        unlink($Anhc);
        move_uploaded_file($_FILES['hinh']['tmp_name'], $path);
    }
    else $Anh = $Anhc;
    $q = "UPDATE sanpham SET tensp = '$Ten', dongia = '$Dg', mota = '$Mt', soluong = '$Sl', hinh = '$Anh', manhom = '$Nhom' WHERE masp = '$Masp'";
    $r = $conn->query($q);
        if(!$r) echo 'Cau truy van bi sai';
        else
        {
            $_SESSION['thongbaoAddSP'] = "Thay đổi Thông tin SP Thành Công!";
            $_SESSION['tieude'] = "Quản lý sản phẩm";                  
            unset($_SESSION['masp']);
            header ("location:adminHome.php?loadpage=QLSanpham.php");
        }
}
if(isset($_POST['DuyetDonHang']))
{
    $madon = $_SESSION['mdh'];
    $nvgiao = $_POST['nvgiao'];
    $nvduyet = $_SESSION['manv'];
    $ngaygiao = $_POST['ngaygiao'];
    $tt = "2";
    $query = "UPDATE donhang SET nvgiao = '$nvgiao', nvduyet = '$nvduyet',ngaygiao ='$ngaygiao',tinhtrang ='$tt' WHERE madon = '$madon' ";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    else
    {
        $_SESSION['thongbaoQLdon_chuaduyet'] = "Duyệt thành công!";
        $_SESSION['tieude'] = "Quản lý đơn hàng đang chờ duyệt";
        unset($_SESSION['mdh']);
        header ("location:adminHome.php?loadpage=QlDon_ChuaDuyet.php");
        
    }    
}
if(isset($_POST['addNhanvien']))
{
    $Pas = $_POST['matkhau'];
    $Repas = $_POST['rematkhau'];
    if($Pas == $Repas)
    {
        require_once('Functions.php');
        $Manv = autoID("nv");
        $Tentk = $_POST['tendangnhap'];
        $Sdt = $_POST['sdt'];
        $Tennv = $_POST['hotennv'];
        $Gioitinh = $_POST['gioitinh'];
        $Email = $_POST['email'];
        $Quyen = $_POST['quyen'];
        $Mk = md5($Pas . $key);

            $query = "SELECT* FROM khachhang WHERE tendangnhap = '$Tentk'";
            $result = $conn->query($query);
            $q = "SELECT* FROM nhanvien WHERE tendangnhap = '$Tentk'";
            $r = $conn->query($q);
            if(!$result || !$r) echo 'Cau truy van bi sai';

            if($result->num_rows ==0 && $r->num_rows ==0)
            {
                $query = "INSERT INTO nhanvien(manv,hotennv,gioitinh,email_nv,sdt,quyen,tendangnhap,matkhau)VALUES('$Manv','$Tennv','$Gioitinh','$Email','$Sdt','$Quyen','$Tentk','$Mk')";
                if($conn->query($query)==true)
                {
                    $_SESSION['thongbaoQlNV'] = "Thêm nhân viên thành công!";
                    header ("location:adminHome.php?loadpage=QLnhanvien.php");
                }
                else "thất bại";
            }
            else
            {
                $_SESSION['thongbaoAddNV'] = "Tên đăng nhập đã có người sử dụng!";
                header ("location:adminHome.php?loadpage=AddNhanvien.php");
            }     
    }
    else
    {
        $_SESSION['thongbaoAddNV'] = "Mật khẩu không kớp! vui lòng nhập lại.";
        header ("location:adminHome.php?loadpage=AddNhanvien.php");
    }
}
if(isset($_POST['register']))
{
    $Pas = $_POST['mk'];
    $Repas = $_POST['remk'];
    if($Pas == $Repas)
    {
        require_once('Functions.php');
        $Makh = autoID("kh");
        $Tentk = $_POST['tk'];
        $Sdt = $_POST['sdt'];
        $Tenkh = $_POST['hoten'];
        $GT = $_POST['gioitinh'];
        $Email = $_POST['email'];
        $Diachi = $_POST['diachi'];
        $Mk = md5($Pas . $key);
        
        $query = "SELECT* FROM khachhang WHERE tendangnhap = '$Tentk'";
        $result = $conn->query($query);
        $q = "SELECT* FROM nhanvien WHERE tendangnhap = '$Tentk'";
        $r = $conn->query($q);
        if(!$result || !$r) echo 'Cau truy van bi sai';

        if($result->num_rows ==0 && $r->num_rows ==0)
        {
            $query = "INSERT INTO khachhang(makh,tenkh,gioitinh,email_kh,sdt,diachi,tendangnhap,matkhau)VALUES('$Makh','$Tenkh','$GT','$Email','$Sdt','$Diachi','$Tentk','$Mk')";
                if($conn->query($query)==true)
                {                    
                    $_SESSION['makh'] = $Makh;
                    $_SESSION['tenkh'] = $Tenkh;
                    header ("location:index.php");                   
                }
                else "thất bại";
        }
        else
            {
                $_SESSION['thongbaoregister'] = "Tên đăng nhập đã có người sử dụng!";
                header ("location:Register.php");
            }
    }
    else
    {
        $_SESSION['thongbaoregister'] = "Mật khẩu không kớp! vui lòng nhập lại.";
        header ("location:Register.php");
    }

}
if(isset($_POST['dangnhap']))
{
    $user = $_POST['tk'];
    $mk = $_POST['mk'];
    $pas = md5($_POST['mk'] . $key);
    
    $query = "SELECT* FROM khachhang WHERE tendangnhap = '$user' and matkhau = '$pas'";
    $result = $conn->query($query);
    if(!$result) echo 'Cau truy van bi sai';
    $row = $result->fetch_assoc();

    if($result->num_rows ==1)
    {
        if($_POST['ghinho'] == true)
        {
                setcookie( "tk", "", time()- 60, "/","", 0);
                setcookie( "mk", "", time()- 60, "/","", 0);
                setcookie("tk", "$user", time() + 600, "/");
                setcookie("mk", "$mk", time() + 600, "/");
        }
        $_SESSION['makh'] = $row['makh'];
        $_SESSION['tenkh'] = $row['tenkh'];
        header ("location:index.php");
    }
    else
    {
        $q = "SELECT* FROM nhanvien WHERE tendangnhap = '$user' and matkhau = '$pas'";
        $r = $conn->query($q);
        if(!$r) echo 'Cau truy van bi sai';
        $row = $r->fetch_assoc();

        if($r->num_rows ==1)
        {
            if($_POST['ghinho'] == true)
            {
                setcookie( "tk", "", time()- 60, "/","", 0);
                setcookie( "mk", "", time()- 60, "/","", 0);
                setcookie("tk", "$user", time() + 600, "/");
                setcookie("mk", "$mk", time() + 600, "/");
            }
            $_SESSION['manv'] = $row['manv'];
            $_SESSION['tennv'] = $row['hotennv'];
            if($row['quyen'] == 3)
            {
                $_SESSION['quyen'] = $row['quyen'];
                header ("location:ShiperHome.php");
            }
            else
            {
                $_SESSION['quyen'] = $row['quyen'];            
                header ("location:adminHome.php");
            }            
        }
        else header('location:login.php?mes=1');        
    }
}
if(isset($_GET['logout']))
{
    session_destroy();
    header ("location:index.php");
}
?>