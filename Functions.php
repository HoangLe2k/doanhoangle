<?php
function autoID($loai)
{
    include('conf.php');
    switch($loai)
    {
        case "nv": 
            $max = 0;
            $query = "SELECT* FROM nhanvien";
            $result = $conn->query($query);
            while ($row = $result->fetch_assoc())
            {
                if(substr($row['manv'], 2) > $max) $max = substr($row['manv'], 2);
            }
            if(($max+1) < 10) return "NV0".$max+1;
            else return "NV".$max+1;
            break;
        case "kh":
            $max = 0;
            $q = "SELECT* FROM khachhang";
            $r = $conn->query($q);
            while ($row = $r->fetch_assoc())
            {
                if(substr($row['makh'], 2) > $max) $max = substr($row['makh'], 2);
            }
            if((int)(($max+1)/10) == 0) return "KH0".$max+1;
            else return "KH".$max+1;
            break;
        case "nsp":
            $max = 0;
            $q = "SELECT* FROM nhomsp";
            $r = $conn->query($q);
            while ($row = $r->fetch_assoc())
            {
                if(substr($row['manhom'], 3) > $max) $max = substr($row['manhom'], 3);
            }
            return "NSP".$max+1;
            break;
        case "sp":
            $max = 0;
            $q = "SELECT* FROM sanpham";
            $r = $conn->query($q);
            while ($row = $r->fetch_assoc())
            {
                if(substr($row['masp'], 2) > $max) $max = substr($row['masp'], 2);
            }
            return "SP".$max+1;
            break;            
        case "dh":
            $max = 0;
            $q = "SELECT* FROM donhang";
            $r = $conn->query($q);
            while ($row = $r->fetch_assoc())
            {
                if(substr($row['madon'], 2) > $max) $max = substr($row['madon'], 2);
            }
            if(($max+1) <9 ) return "HD00".$max+1;
            if(($max+1) <99 && ($max+1) > 9 ) return "HD0".$max+1;
                else return "HD".$max+1;
            break;
    }
}
?>