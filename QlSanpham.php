<div class="container">
<form method="POST" action="action.php">
    <div class="form-group">
        <table>
            <tr>
                <td colspan =3><input size=200px type="text" name="timQLSP" class="form-control" placeholder="Tìm kiếm..."></td>
                <td><input  type="submit" name="timkiemQLSP" class="btn btn-primary btn-block" value="Tìm kiếm"></td>
            </tr>
        </table>      
                
    </div>
    <?php
        include('action.php');
        if(isset($_SESSION['timkiemQLSP']))
        {
            $key = $_SESSION['timkiemQLSP'];
            $query = "SELECT * FROM nhomsp WHERE manhom LIKE '$key' OR tennhom LIKE '$key'";
            $q = "SELECT * FROM sanpham,nhomsp WHERE sanpham.manhom = nhomsp.manhom and (masp LIKE '$key' or nhomsp.tennhom LIKE '$key' or tensp LIKE '$key' or mota LIKE '$key'  ) ORDER BY sanpham.soluong";
            unset($_SESSION['timkiemQLSP']);
        } 
        else
        {
            $query = "SELECT * FROM nhomsp";
            $q = "SELECT * FROM sanpham,nhomsp WHERE sanpham.manhom = nhomsp.manhom ORDER BY sanpham.soluong";
        }
        $result = $conn->query($query);
        if(!$result) echo 'Cau truy van bi sai';
    ?>
    <h3 class="text-center text-info">Danh sách loại sản phẩm</h3>
    <?php if(isset($_SESSION['thongbaoAddNSP']))
        {
            echo '<div class="form-group">
            <span style="color:red">'.$_SESSION['thongbaoAddNSP'].'</span>
            </div>';
            unset($_SESSION['thongbaoAddNSP']);
        } ?>
    <div class="form-group">
        <input  type="submit" name="goAddNhomsp" class="btn btn-primary btn-block" value="Thêm nhóm sản phẩm">            
    </div>
    <table class="table table-hover" id="data-table">
        <thead>
        <tr bgcolor="#95f461">
            <th>Mã Loại sản phẩm</th>
            <th>Tên Loại</th>
            <?php
                if($_SESSION['quyen'] != 3) echo "<th>Hành Động</th>";
            ?>
        </tr>
        </thead>          
        <tbody>
        <?php $d=0; while ($row = $result->fetch_assoc()) {$d++;
            if($d%2==1) $bg="#b0e5e5"; else $bg= "white";
            ?>
        <tr bgcolor="<?php echo $bg; ?>">
            <td><?= $row['manhom']; ?></td>
            <td><?= $row['tennhom']; ?></td>
            <?php
            if($_SESSION['quyen'] != 3)
            {
                $mnsp = $row['manhom'];
                echo "<td><a style='background-color: #ff7070;' class='btn btn-primary' href='action.php?xoaNSP=$mnsp'>Xóa</a></td>";
                                
            } 
            ?>        
        </tr>
        <?php } ?>
        </tbody>
    </table>    
    <h3 class="text-center text-info">Danh sách sản phẩm</h3>
    <?php if(isset($_SESSION['thongbaoAddSP']))
        {
            echo '<div class="form-group">
            <span style="color:red">'.$_SESSION['thongbaoAddSP'].'</span>
            </div>';
            unset($_SESSION['thongbaoAddSP']);
        } ?>
    <div class="form-group">
        <input  type="submit" name="goAddSanpham" class="btn btn-primary btn-block" value="Thêm sản phẩm">            
    </div>
    <table class="table table-hover" id="data-table">
        <thead>
        <tr bgcolor="#95f461">
            <th>Ảnh minh họa</th>
            <th>Mã sản phẩm</th>
            <th>Tên sản phẩm</th>
            <th>Đơn giá</th>
            <th style="text-align: center;">Mô tả</th>
            <th>Số Lượng còn trong kho</th>
            <th>Tên loại sản phẩm</th>
            <?php
                if($_SESSION['quyen'] != 3) echo "<th>Hành Động</th>";
            ?>
        </tr>
        </thead>    
        <tbody>
        <?php
            $r = $conn->query($q);
            if(!$r) echo 'Cau truy van bi sai';
            $d=0; while ($row2 = $r->fetch_assoc()) {$d++;
            if($row2['soluong']==0) $bg="#f27171";
            else
            {
                if($d%2==1) $bg="#b0e5e5"; else $bg= "white";
            }            
            ?>
        <tr bgcolor="<?php echo $bg; ?>">
            <td><img style="width: 100px;" src="./images/<?= $row2['hinh']; ?>" ></td>
            <td><?= $row2['masp']; ?></td>
            <td><?= $row2['tensp']; ?></td>
            <td><?= $row2['dongia']; ?></td>
            <td><?= $row2['mota']; ?></td>
            <td><?= $row2['soluong']; ?></td>
            <td><?= $row2['tennhom']; ?></td> 
            <?php
            if($_SESSION['quyen'] != 3)
            {
                $msp = $row2['masp'];
                echo "<td><a href='action.php?chitietSP=$msp' class='badge badge-primary p-2'>Chi tiết</a>
                <a style='background-color: #fc3232;' href='action.php?xoaSP=$msp' class='badge badge-primary p-2'>Xóa SP</a></td>";
            }
            ?>   
        </tr>
        <?php } ?>
        </tbody>
    </table>
    
</form>
</div>